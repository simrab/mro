This repository contains all the programs generating the data and the results of the paper 

*Can I stay or should I go? Mandatory retirement and the labor-force participation of older workers* (Journal of Public economics, 2019)
[**Link**](https://www.sciencedirect.com/science/article/pii/S0047272719301392?dgcid=author)

In case of remarks or question please contact me directly: s.rabate@cpb.nl
