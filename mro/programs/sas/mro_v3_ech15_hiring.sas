/***************************************************************************************************************************************************************************
*
*  MRO: version 3. PART 1: Echantillon au 20e
*  We keep only work history, merge with NAF is done on R. 
*  Two datesets are saved: one with (fixed) individual information and the other with all the work history between 

/* V3: correction calcul DA602 + ajouts variables de controle */

/*connexion  */                                                                                                           
%let srv=Sasdspr.n18.an.cnav 7551;                                                                                 
option comamid=tcp remote=srv;                                                                                             
signon noscript user= z078886 password= _prompt_;                                                                           

rsubmit;
libname xtrav '/sasdata/travail8/z078886';
endrsubmit;

libname trav slibref=xtrav server=srv;
libname xwork slibref=work server=srv;


rsubmit;
data sal_min (keep=start label fmtname);
      set xtrav.Sal_valid_1trim_150_20170721;
      start=      an_sal*10000 + dt_ej_deb;
      label = sal_euro_2010;
      fmtname = 'h150sal_min';

run;
proc format lib=work.formats cntlin=sal_min; run;
proc datasets nolist library=work; delete sal_min; run;
endrsubmit;


%macro exportxls (fichier,chemin,onglet,typop);
proc export data=xwork.&fichier.
   outfile=&chemin.
   dbms=excel &typop.;
   sheet=&onglet.;
run;
%mend;



/***** I. Work on data ********/


*** I.1  filters ***;
/* Filters 1:  Variables + generations (until 1950)*/
rsubmit;
%macro filters1();
data work.init;
set xtrav.Table_init_ech15_20e_avtcomplet (keep=    ident sexe pays_nais prem_rep nb_enf generation tr_rg_30_46
 trim_nais mois_nais an_deces prem_jour an_ej trim_ej mois_ej valid_tot valid_rg categ trim_surcote
       %do i=1947 %to 2015;  rep_&i._1--tr_&i._4  plaf_&i.--avpf_&i.  %end ; );
if generation>=1934  & generation<1960;
run;
%mend;      
endrsubmit;
rsubmit;
%filters1();
endrsubmit;

/* Filters 2: keep those with RG as the last employer + control variables on last 15 years */
rsubmit;
data work.temp2;
set  work.init; 
/* Arrays */
array sal(1947:2015,3) plaf_1947--avpf_2015 ;
array rep(1947:2015,2,4) rep_1947_1--tr_2015_4 ;
* Variables;
an_finact   =0;  * year of last report;
age_finact  =0;  * age of last report;
last_rep    =''; * type of last report;    
last_sal    =0;  * last sal before 55;
an_60       =0;  * year of 60;
nb_rep_last15      =0;  * nb years with employment over 15 last years;
sal_tot    =0;  * total sal over 15 last years;
mean_sal_last15    =0;  * mean sal of last 15 years;
do a = 1970 to 2015; * Loop on years;
age=a-generation;
* Type of last report before liq;
if (a<an_ej or an_ej=.) and (rep(a,1,1)='e' or rep(a,1,1)='h' or rep(a,1,1)='k')  then  last_rep = rep(a,1,1);
* Year of 60 and last report if retired;
if age=60                      then an_60=a;
if sal(a,1)>0 and age<=55      then last_sal=sal(a,1);
    if sal(a,1)>0 and (a<=an_ej or an_ej=.)     then do;
	an_finact=a; 
	age_finact=age;  
	end;
	if age>=45 and age<60 then do;
	if sal(a,1)>0 then nb_rep_last15=nb_rep_last15+1;
	if sal(a,1)>0 then sal_tot=sal_tot+sal(a,1);
	end;
end;
if (nb_rep_last15>0) then mean_sal_last15 = sal_tot/nb_rep_last15;
run;
endrsubmit;


rsubmit;
data work.temp;
set  work.temp2; 
if last_rep ='e';
if age_finact>=50;
run;
endrsubmit;




rsubmit;
data workers;
set  temp; 
run;
endrsubmit;





/*** I.4 Merge with employers information ***/

/** I.4.a Merge datasets */ 
rsubmit;
%macro filters3();
data work.emp;
set xtrav.ech2015_enreg6_hades (keep=    ident  %do i=2000 %to 2015;  
								nu_emp_&i._1 nu_emp_&i._2 act_etab_&i._1 act_etab_&i._2 cd_emp_&i._1  cd_emp_&i._2 raison_sociale_&i._1  raison_sociale_&i._2 
								cd_ape_etab_&i._1  dt_fin_&i._1 cd_ape_etab_&i._2 dt_fin_&i._2 na_dcl_sal_&i._1 na_dcl_sal_&i._2 cd_lieu_rsd_&i._1 cd_lieu_rsd_&i._2 %end ; );
* Recode when 1 is MV and 2 is not;
%do i=2000 %to 2015;
%let j=%eval(&i.-1);
if (cd_ape_etab_&i._1='' and cd_ape_etab_&i._2 ne '') then do;
cd_ape_etab_&i._1=cd_ape_etab_&i._2;
cd_emp_&i._1=cd_emp_&i._2;  
dt_fin_&i._1=dt_fin_&i._2;
cd_lieu_rsd_&i._1=cd_lieu_rsd_&i._2;
raison_sociale_&i._1= raison_sociale_&i._2;
na_dcl_sal_&i._1= na_dcl_sal_&i._2;
end;
naf_&i=cd_ape_etab_&i._1;
dep_&i=cd_lieu_rsd_&i._1;
%end;
run;
%mend;      
endrsubmit;
rsubmit;
%filters3();
endrsubmit;

rsubmit;
data to_correct;
set  emp;
if ((raison_sociale_2000_1="URSSAF" and na_dcl_sal_2000_1="06") or (raison_sociale_2001_1="URSSAF" and na_dcl_sal_2001_1="06") or (raison_sociale_2002_1="URSSAF" and na_dcl_sal_2002_1="06"));
run;
endrsubmit;
rsubmit;
proc freq data=to_correct;
table nu_emp_2000_1 nu_emp_2001_1 nu_emp_2002_1/missing;
run;
endrsubmit;



** Correction !! employ�s de maison enregistr�s � l'URSAFF avant 2002! *;
rsubmit;
%macro correctNAF();
data emp;
set  emp;
%do i=2000 %to 2002;
if (raison_sociale_&i._1="URSSAF" and na_dcl_sal_&i._1="06") then do;
raison_sociale_&i._1="";
naf_&i.="";
end;
%end;
run;
%mend;      
endrsubmit;
rsubmit;
%correctNAF();
endrsubmit;






rsubmit;
data base;
merge workers  (in=main)
	  emp      (in=b);
by ident; 
if main;
run;
endrsubmit;


rsubmit;
data xtrav.mro1;
set base ;
run;
endrsubmit;



/* check : sal>0 et no naf*/
rsubmit;
data check ;
set base;
if plaf_2010>0;
run;
proc sort data=check; by naf ; run;
proc freq data=check;
table naf_2010 /missing;
run;
endrsubmit;


/**** II. Save output ****/

libname O 'O:\bunching\MRO\data\';
libname N 'N:\DSPR-Simon\data\';
libname F 'F:\data\raw';


/**** II.1 Individual data ****/
data F.data_ind_ech15_hiring_v1;
 set trav.mro1 (keep= ident sexe  generation pays_nais trim_nais mois_nais  prem_jour an_ej trim_ej mois_ej valid_tot valid_rg 
					   prem_rep mean_sal_last15  nb_rep_last15 an_finact 
                 ); 
run;

rsubmit;
data temp;
set xtrav.mro1;
diff =  trim_sur3  - trim_sur2 ;
run;
proc freq data =  temp;
table diff;
run;
endrsubmit;



/**** II.2 Long data ****/
rsubmit;
%macro filters4();
data temp;
set  xtrav.mro1 (keep=   ident sexe  generation an_ej   an_finact 
       					%do i=2000 %to 2015;   naf_&i. plaf_&i. dep_&i.  %end ; );
run;
%mend;     

endrsubmit;
rsubmit;
%filters4();
endrsubmit;


* long to wide;
rsubmit;
data wide1;
set  temp (keep= ident naf_2000 naf_2001 naf_2002 naf_2003 naf_2004 naf_2005 naf_2006 
				 	   naf_2007 naf_2008 naf_2009 naf_2010 naf_2011 naf_2012 naf_2013 naf_2014 naf_2015 );
run;
proc sort data=work.wide1 ; by ident  ; run;
run;
proc transpose data=work.wide1  out=work.long1;
var naf_2000 naf_2001 naf_2002 naf_2003 naf_2004 naf_2005 naf_2006 
	naf_2007 naf_2008 naf_2009 naf_2010 naf_2011 naf_2012 naf_2013 naf_2014 naf_2015 ;
by ident ;
run;
data long1;
 set long1 (rename=(col1=naf));
 year=input(substr(_name_, 5), 5.);
 drop _name_;
run; 


data wide2;
set  temp (keep= ident plaf_2000 plaf_2001 plaf_2002 plaf_2003 plaf_2004 plaf_2005 plaf_2006 
				 	   plaf_2007 plaf_2008 plaf_2009 plaf_2010 plaf_2011 plaf_2012 plaf_2013 plaf_2014 plaf_2015 );
run;
proc sort data=work.wide2 ; by ident  ; run;
proc transpose data=work.wide2  out=work.long2;
var plaf_2000 plaf_2001 plaf_2002 plaf_2003 plaf_2004 plaf_2005 plaf_2006 
	plaf_2007 plaf_2008 plaf_2009 plaf_2010 plaf_2011 plaf_2012 plaf_2013 plaf_2014 plaf_2015  ;
by ident ;
run;
data long2;
 set long2 (rename=(col1=plaf));
 year=input(substr(_name_, 6), 5.);
 drop _name_;
run; 

data wide3;
set  temp (keep= ident dep_2000 dep_2001 dep_2002 dep_2003 dep_2004 dep_2005 dep_2006 
				 	   dep_2007 dep_2008 dep_2009 dep_2010 dep_2011 dep_2012 dep_2013 dep_2014 dep_2015 );
run;
proc sort data=work.wide3 ; by ident  ; run;
proc transpose data=work.wide3  out=work.long3;
var dep_2000 dep_2001 dep_2002 dep_2003 dep_2004 dep_2005 dep_2006 
	dep_2007 dep_2008 dep_2009 dep_2010 dep_2011 dep_2012 dep_2013 dep_2014 dep_2015 ;
by ident ;
run;
data long3;
 set long3 (rename=(col1=dep));
 year=input(substr(_name_, 5), 5.);
 drop _name_;
run; 



data indiv;
set  temp (keep=     ident sexe  generation an_ej
                           an_finact );


proc sort data=work.long1 ; by ident  ; run;
proc sort data=work.long2 ; by ident  ; run;
proc sort data=work.long3 ; by ident  ; run;
proc sort data=work.indiv ; by ident  ; run;
data long;
merge long1 long2 long3;
by ident; 
run;
data base;
merge long  (in=main)
	  indiv (in=b);
by ident; 
if main;
run;
endrsubmit;

/* Variables: dummies for, retire a given year, claim pension a given year */
rsubmit; 
data base;
set base;
t_liq   = 0;
  liq   = 0;
t_finact= 0;
  finact= 0;
if an_ej=year      then t_liq=1;
if an_ej<=year     then   liq=1;
if an_finact=year  then t_finact=1;
if an_finact<=year then   finact=1;
run;
endrsubmit;




/* check : sal>0 et no naf*/
rsubmit;
data check ;
set base;
if plaf>0;
run;
proc sort data=check; by naf ; run;
proc freq data=check;
table naf /missing;
run;
endrsubmit;



/* drop observations with an_finact after 2000 and age <68 age >59 and year until fin act only*/
rsubmit; 
data base;
set base;
if an_finact>=2000;
if year-generation>=50;
if generation>=1934;
*if year<=an_finact+1;
run;
endrsubmit;

libname N 'N:\DSPR-Simon\data\';
libname O 'O:\bunching\MRO\data\';
libname F 'F:\data\raw';
data F.data_long_ech15_hiring_v1;
set  xwork.base; 
run;



