/***************************************************************************************************************************************************************************
*
*  MRO: TEST FR LOCALISATION *
Pour liquidation avant 2004 (pe 2006), pas de report syst�matique des p�riodes non report�es au moment de la liquidation dans les variables carri�res (rep). 
=> A partir de G46, reconstitution de la carri�re � partir des reports tout au long de la vie sans doute OK.
=> Avant, reconstitution � partir de la dur�e tot est sans doute la seule viable. 
Test: comparaison des deux mesures, et trouver le point � partir du moment o� elle convergent. 
*/

/** Connexion **/
%let srv=SasSrvConnect.n18.an.cnav 7551;
option comamid=tcp remote=srv;
signon noscript user=_prompt_ password=_prompt_;

rsubmit;
libname xtrav '/travail8/z078886';
endrsubmit;
libname trav slibref=xtrav server=srv;
libname xwork slibref=work server=srv;

%INCLUDE "O:\bunching\Echant20e_2014\formats.sas";


%macro exportxls (fichier,chemin,onglet,typop);
proc export data=xwork.&fichier.
   outfile=&chemin.
   dbms=excel &typop.;
   sheet=&onglet.;
run;
%mend;



/***** I. Comparaison of FR for G1947 (late) ********/



*** I.1  filters ***;
/* Filters 1:  Variables + generations (until 1950)*/
rsubmit;
%macro filters1();
data work.init;
set xtrav.Table_init_ech14_20e_avtcomplet (keep=    ident sexe pays_nais prem_rep nb_enf generation tr_rg_30_46
 trim_nais mois_nais an_deces prem_jour an_ej trim_ej mois_ej valid_tot valid_rg categ
       %do i=1947 %to 2014;  rep_&i._1--tr_&i._4  plaf_&i.--avpf_&i.  %end ; );
if generation>=1934  & generation<1955;
run;
%mend;      
endrsubmit;
rsubmit;
%filters1();
endrsubmit;

/* L�gilsation */
rsubmit;
data work.temp2;
set  work.init; 
* Minimum age;
if generation<=1950               then aod=60 ;
if generation=1951 & mois_nais>=7 then aod=60.33;
if generation=1952  then  aod=60.75;
if generation=1953  then  aod=61.17;
if generation=1954  then  aod=61.58;     
if generation>=1955 then  aod=62;    
* Minimum age;
if generation<=1950                    then aad=65 ;
if generation=1951 & mois_nais<=6      then aad=65 ;
if generation=1951 & mois_nais>=7      then aad=65.33 ;
if generation=1952                     then aad=65.75 ;
if generation=1953                     then aad=66.17 ;
if generation=1954                     then aad=66.58 ;
if generation>=1955                    then aad=67 ;
* Required duration;
duree_req=0;
if generation<=1933 & an_ej <2003 then duree_req=150 ;
if generation=1934 & an_ej <2003 then duree_req=151 ;
if generation=1935 & an_ej <2003 then duree_req=152 ;
if generation=1936 & an_ej <2003 then duree_req=153 ;
if generation=1937 & an_ej <2003 then duree_req=154 ;
if generation=1938 & an_ej <2003 then duree_req=155 ;
if generation=1939 & an_ej <2003 then duree_req=156 ;
if generation=1940 & an_ej <2003 then duree_req=157 ;
if generation=1941 & an_ej <2003 then duree_req=158 ;
if generation=1942 & an_ej <2003 then duree_req=159 ;
if an_ej >=2003                  then duree_req=160 ;
if generation=1943               then duree_req=160 ;
if generation=1944               then duree_req=160 ;
if generation=1945               then duree_req=160 ;
if generation=1946               then duree_req=160 ;
if generation=1947               then duree_req=160 ;
if generation=1948               then duree_req=160 ;
if generation=1949               then duree_req=161 ;
if generation=1950               then duree_req=162 ;
if generation=1951               then duree_req=163 ;
if generation=1952               then duree_req=164 ;
if generation=1953               then duree_req=165 ;
if generation=1954               then duree_req=165 ;
if generation=1955               then duree_req=166 ;
run;
endrsubmit;


/*** Carri�re durationa and full rate ***/

** Methode 1 : Computation of ID60 and an_FR using the retrospective calendar  **;
rsubmit;
data work.temp;
set  work.temp; 
/* Arrays */
array rep(1947:2014,2,4) rep_1947_1--tr_2014_4 ;
array sal(1947:2014,4)   plaf_1947--avpf_2014  ;
/* Initialisation */
ID602        = valid_tot; 
count_valid2 = valid_tot; 
an_dureereq = 5555; *year at which the required duration is reached;
age_dureereq2 = 65;
* Loop on years;
do a = 2014 to 1980 by -1; 
   age_ann = a - generation;
   incr1 =0;
   incr2 =0;
   mil=0;mal=0;cho=0;cot=0;ali=0;nali=0;avp=0;equ=0;aut=0;inv=0;cot2=0; 
   do t=1 to 4;       * Loop on nb of reports ;
   select (rep(a,1,t));
                        when ('a')    mil=sum(mil,input(rep(a,2,t),2.)); 
                        when ('b')    mal=sum(mal,input(rep(a,2,t),2.));
                        when ('c')    cho=sum(cho,input(rep(a,2,t),2.));
                        when ('h')    ali=sum(ali,input(rep(a,2,t),2.));
                        when ('k')    nali=sum(nali,input(rep(a,2,t),2.));
                        when ('g')    equ=sum(equ,input(rep(a,2,t),2.));
                        when ('d')    aut=sum(aut,input(rep(a,2,t),2.));
                        when ('e')    cot2=sum(cot2,input(rep(a,2,t),2.));
                        otherwise;
                        end;
     end; * end loop on nb of reports;
     if sal(a,1)>0 then cot=int(sal(a,1)/input(put(a*10000 + sum( an_ej, 0.25*int(sum(mois_ej,-1)/3 +1) ) * ( 0<an_ej<5555 and a<an_ej ) ,h150sal_min.),best12.));
     if sal(a,4)>0 then avp=int(sum(sal(a,1),sal(a,4))/input(put(a*10000 + sum( an_ej, 0.25*int(sum(mois_ej,-1)/3 +1) ) * ( 0<an_ej<5555 and a<an_ej ) ,h150sal_min.),best12.));
	incr1= min(sum(0,mil,mal,cho,cot*(avp<=0),avp,ali,nali,equ,aut,inv),4);
	incr2= min(sum(0,mil,mal,cho,cot2        ,avp,ali,nali,equ,aut,inv),4);
    /* Increment for an_FR*/
	* Test if duree_req reached this year;
	if count_valid2>= duree_req then do;
	an_dureereq2= a;
	age_dureereq2=age_ann;
	end;
	* Increment (backward) of the duration);
	if a = an_ej then  count_valid2      =sum( count_valid2 , -min(incr1,trim_ej-1));
	if a < an_ej then  count_valid2      =sum( count_valid2 ,-incr1);
	/* Increment dependent on if age=60 or a=an_ej*/
	if      a = an_ej   then ID602   =sum( ID602       , -min(incr1,trim_ej-1));
	else if age_ann> 60 then ID602   =sum ( ID602      , -incr1);
	else if age_ann= 60 then ID602   =sum( ID602       , -min(incr1,trim_nais-1));
 end;  * end loop on year;
* Age and an TR ;
age_FR2=max(60,	age_dureereq2);
age_FR2=min(65,	age_FR2);
an_FR2 = generation + age_FR2;
run;
endrsubmit;

*** Methode 2 : Computation of ID60 and an_FR from the beginning ***;
rsubmit;
data work.temp;
set  work.temp; 
/* Arrays */
array rep(1947:2014,2,4) rep_1947_1--tr_2014_4 ;
array sal(1947:2014,4)   plaf_1947--avpf_2014  ;
/* Initialisation */
age_dureereq3 = 0;
ind=0;
ID603        = sum(0,nb_enf)*8*(sexe='2')+sum(0,tr_rg_30_46); 
IDrg603      = sum(0,nb_enf)*8*(sexe='2')+sum(0,tr_rg_30_46); 
count_valid3  = sum(0,nb_enf)*8*(sexe='2')+sum(0,tr_rg_30_46); 
do a = 1947 to 2014; * Loop on years;
   age_ann = a - generation;
   incr1 =0;
   incr2 =0;
   mil=0;mal=0;cho=0;cot=0;ali=0;nali=0;avp=0;equ=0;aut=0;inv=0;cot2=0; 
   do t=1 to 4;       * Loop on nb of reports ;
   select (rep(a,1,t));
                        when ('a')    mil=sum(mil,input(rep(a,2,t),2.)); 
                        when ('b')    mal=sum(mal,input(rep(a,2,t),2.));
                        when ('c')    cho=sum(cho,input(rep(a,2,t),2.));
                        when ('h')    ali=sum(ali,input(rep(a,2,t),2.));
                        when ('k')    nali=sum(nali,input(rep(a,2,t),2.));
                        when ('g')    equ=sum(equ,input(rep(a,2,t),2.));
                        when ('d')    aut=sum(aut,input(rep(a,2,t),2.));
                        when ('e')    cot2=sum(cot2,input(rep(a,2,t),2.));
                        otherwise;
                        end;
     end; * end loop on nb of reports;
     if sal(a,1)>0 then cot=int(sal(a,1)/input(put(a*10000 + sum( an_ej, 0.25*int(sum(mois_ej,-1)/3 +1) ) * ( 0<an_ej<5555 and a<an_ej ) ,h150sal_min.),best12.));
     if sal(a,4)>0 then avp=int(sum(sal(a,1),sal(a,4))/input(put(a*10000 + sum( an_ej, 0.25*int(sum(mois_ej,-1)/3 +1) ) * ( 0<an_ej<5555 and a<an_ej ) ,h150sal_min.),best12.));
	incr1= min(sum(0,mil,mal,cho,cot2*(avp<=0),avp,ali,nali,equ,aut,inv),4);
	incr2= min(sum(0,mil,mal,cho,cot2*(avp<=0),avp,aut,inv),4) ;
	/* Increment for duration at 60 */
	if age_ann< 60 then do;
    ID603=sum( ID603 , incr1);
    IDrg603 =sum (IDrg603  , incr2);
	end;
	if age_ann= 60 then do;
    ID603   =sum( ID603  , min(incr1,trim_nais));
    IDrg603 =sum (IDrg603, min(incr2,trim_nais));
	end;           
	/* Increment for an_FR*/
	* Increment of the duration;
	if a = an_ej  then do;
	count_valid3      =sum( count_valid3 , min(incr1,trim_ej-1));
	end;
	if (a < an_ej or an_ej=.) then  do;
	count_valid3      =sum( count_valid3 , incr1);
	end;
	* Test if duree_req reached this year;
	if count_valid3<= duree_req then do;
	an_dureereq3= a;
	age_dureereq3=age_ann;
	end;
 end; 
* Age and an TR ;
age_FR3 = max(60,	age_dureereq3);
age_FR3 = min(65,	age_FR3);
an_FR3  = generation + age_FR3;
run;
endrsubmit;








/***** I. From xtrav.mro1 ********/

data temp (keep= ident generation durvalap602 durvalap603 sexe an_ej  valid_tot ID602 ID603 age_FR2 age_FR3 diff_age_TR group2 group3 				 
                 );
 set xtrav.mro1; 
diff_age_TR = age_FR3-age_FR2;
diff_ID60   = ID603-ID602;
durvalap603 = valid_tot-ID603;
durvalap602 = valid_tot-ID602;
diff_validtot = valid_tot-count_valid3;
run;

/***** CHECK: comparison FR2 and FR3 ****/
proc freq data=temp (where=(generation=1946 and sexe='1'));
table age_FR2 /missing;
run;
proc freq data=temp (where=(generation=1946 and sexe='1'));
table age_FR3 /missing;
run;

rsubmit;
data temp;
set  temp;
diff_age_TR = age_FR3-age_FR2;
diff_ID60   = ID603-ID602;
durvalap603 = valid_tot-ID603;
durvalap602 = valid_tot-ID602;
diff_validtot = valid_tot-count_valid3;
run;
endrsubmit;

rsubmit;
proc means data=temp (where=(sexe="1" and generation=1945 and an_ej^=.));
var diff_age_TR diff_ID60 durvalap603 durvalap602 diff_validtot; 
run;
proc freq data=temp (where=(sexe="1" and generation=1945 and an_ej^=.));
table diff_age_TR /missing;
run;
endrsubmit;


/*** Check on those with diff_age=5 ie AGE_FR3=65 and AGE_FR2=60 



rsubmit;
data check;
set temp;
if age_FR3=65 and age_FR2=60 and sexe="1" and generation=1940 and an_ej^=.;
run;
endrsubmit;




rsubmit;
proc freq data=temp (where=(sexe="1" and generation=1940));
table an_ej /missing;
run;
endrsubmit;

rsubmit;
proc freq data=temp;
table age_FR1 /missing;
run;
proc freq data=temp;
table age_FR2 /missing;
run;
proc freq data=temp;
table age_FR3 /missing;
run;
endrsubmit;



rsubmit;
proc freq data=temp;
table ID601 /missing;
run;
proc freq data=temp;
table ID602 /missing;
run;
proc freq data=temp;
table ID603 /missing;
run;
endrsubmit;



