/***************************************************************************************************************************************************************************


/*connexion  */ 
%let srv=Sasdspr.n18.an.cnav 7551;                                                                                 
option comamid=tcp remote=srv;                                                                                             
signon noscript user= z078886 password= _prompt_;                                                                           

rsubmit;
libname xtrav '/sasdata/travail8/z078886';
endrsubmit;

libname trav slibref=xtrav server=srv;
libname xwork slibref=work server=srv;


/******* 2015 ********/
rsubmit;
data work.echant15;
set xtrav.Table_init_ech15_20e_avtcomplet (keep=    ident generation deplaf_2002 deplaf_2003 deplaf_2004 deplaf_2005);
if generation>=1934  & generation<1945;
run;
data work.hades15;
set xtrav.ech2015_enreg6_hades (keep=    ident  cd_ape_etab_2002_1 cd_ape_etab_2003_1 cd_ape_etab_2004_1 cd_ape_etab_2005_1);
run;
data base15;
merge echant15  (in=main)
	  hades15   (in=b);
by ident; 
if main;
run;
endrsubmit;

/*** Missing naf vs deplaf ***/
rsubmit;
data base15 ;
set base15;
no_plaf_2002 = 0; no_plaf_2005 = 0;
if (cmiss(deplaf_2002) or deplaf_2002 = 0) then  no_plaf_2002 = 1;
if (cmiss(deplaf_2005) or deplaf_2005 = 0) then  no_plaf_2005 = 1;
no_naf_2002 = 0;no_naf_2005 = 0;
if cd_ape_etab_2002_1 = "" then no_naf_2002 = 1;
if cd_ape_etab_2005_1 = "" then no_naf_2005 = 1;
run;
proc freq data = base15; 
table no_plaf_2002*no_naf_2002; 
run;
proc freq data = base15; 
table no_plaf_2005*no_naf_2005; 
run;
endrsubmit;


/******* 2016 ********/
rsubmit;
data work.echant16;
set xtrav.Table_init_ech16_20e_avtcomplet (keep=    ident generation deplaf_2002 deplaf_2003 deplaf_2004 deplaf_2005);
if generation>=1934  & generation<1945;
run;
data work.hades16;
set xtrav.ech2016_enreg6_hades (keep=    ident  cd_ape_etab_2002_1 cd_ape_etab_2003_1 cd_ape_etab_2004_1 cd_ape_etab_2005_1);
run;
data base16;
merge echant16  (in=main)
	  hades16   (in=b);
by ident; 
if main;
run;
endrsubmit;

/*** Missing naf vs deplaf ***/
rsubmit;
data base16 ;
set base16;
no_plaf_2002 = 0; no_plaf_2005 = 0;
if (cmiss(deplaf_2002) or deplaf_2002 = 0) then  no_plaf_2002 = 1;
if (cmiss(deplaf_2005) or deplaf_2005 = 0) then  no_plaf_2005 = 1;
no_naf_2002 = 0;no_naf_2005 = 0;
if cd_ape_etab_2002_1 = "" then no_naf_2002 = 1;
if cd_ape_etab_2005_1 = "" then no_naf_2005 = 1;
run;
proc freq data = base16; 
table no_plaf_2002*no_naf_2002; 
run;
proc freq data = base16; 
table no_plaf_2005*no_naf_2005; 
run;
endrsubmit;


