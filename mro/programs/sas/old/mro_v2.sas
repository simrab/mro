/***************************************************************************************************************************************************************************
*
*  MRO: premi�re exploration statistique.
*  Avant/apr�s 2003 pour les d�parts en retraite selon accord de branche ou pas.  
* PLOT: - proba de d�part en dessous de 65 ans entre 2000 et 2009 avant/apr�s 2003 avec/sans convention. 
		- proba de d�part au TP entre 2000 et 2009 avant/apr�s 2003 avec/sans convention (� 60 ans et apr�s)
***************************************************************************************************************************************************************************/


/** Connexion **/
%let srv=SasSrvConnect.n18.an.cnav 7551;
option comamid=tcp remote=srv;
signon noscript user=_prompt_ password=_prompt_;

rsubmit;
libname xtrav '/travail8/z078886';
endrsubmit;
libname trav slibref=xtrav server=srv;
libname xwork slibref=work server=srv;


/***** I. Work on data ********/

/*** I.1 Carrer
Sample selection: we keep only individuals:
- retiring between 2000 and 2010  
- with last employer=RG
***/


** I.1.a filters;
/* Filters 1:  Variables + generations (keep until 55 in 2013)*/
rsubmit;
%macro filters1();
data work.temp;
set xtrav.Table_init_ech14_20e_avtc_eval (keep=    ident sexe nb_enf generation trim_nais mois_nais an_deces prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali 
       %do i=1947 %to 2014;  rep_&i._1--tr_&i._4  plaf_&i.  %end ; );
if generation>="1932";
if generation<="1958";
run;
%mend;      
endrsubmit;
rsubmit;
%filters1();
endrsubmit;

/* Filters 2: keep those with RG as the last employer */
rsubmit;
data work.temp2;
set  work.temp; 
array sal(1947:2014,1) plaf_1947--plaf_2014 ;/* Arrays */
array rep(1947:2014,2,4) rep_1947_1--tr_2014_4 ;
an_lastw    =0;  * year of last report;
last_rep    =''; * type of last report;
an_60       =0;  * year of 60;
do a = 1970 to 2014; * Loop on years;
age=a-generation;
/* Type of last report before liq */
if (a<an_ej or an_ej=.) and (rep(a,1,1)='e' or rep(a,1,1)='h' or rep(a,1,1)='k')  then  last_rep = rep(a,1,1);
/* Year of 60 and last report if retired */
if age=60                      then an_60=a;
if sal(a,1)>0 and (a<=an_ej or an_ej=.)     then an_lastw=a;  
end;
run;
endrsubmit;




rsubmit;
data work.temp;
set  work.temp2; 
if last_rep ='e';
run;
endrsubmit;


** I.1.b variables creation;
rsubmit;
data work.temp;
set  work.temp; 
* Retirement age; 
ageliq_month = ( an_ej+(mois_ej-1)/12 ) -  (generation + (mois_nais-1)/12 );
if prem_jour^='1' then ageliq_month=ageliq_month-1/12;
t = ageliq_month-floor(ageliq_month);
t = round(t*12)+1  ;
t = floor((t-1)/3) ;
ageliq_trim2 = floor(ageliq_month) + t*0.25 ;
ageliq_ann   = floor(ageliq_trim2);
ID601        = valid_tot - max(0,(ageliq_trim2-60)*4);
* required duration;
duree_req=0;
if generation<=1933 & an_ej <2003 then duree_req=150 ;
if generation=1934 & an_ej <2003 then duree_req=151 ;
if generation=1935 & an_ej <2003 then duree_req=152 ;
if generation=1936 & an_ej <2003 then duree_req=153 ;
if generation=1937 & an_ej <2003 then duree_req=154 ;
if generation=1938 & an_ej <2003 then duree_req=155 ;
if generation=1939 & an_ej <2003 then duree_req=156 ;
if generation=1940 & an_ej <2003 then duree_req=157 ;
if generation=1941 & an_ej <2003 then duree_req=158 ;
if generation=1942 & an_ej <2003 then duree_req=159 ;
if an_ej >=2003                  then duree_req=160 ;
if generation=1943               then duree_req=160 ;
if generation=1944               then duree_req=160 ;
if generation=1945               then duree_req=160 ;
if generation=1946               then duree_req=160 ;
if generation=1947               then duree_req=160 ;
if generation=1948               then duree_req=160 ;
if generation=1949               then duree_req=161 ;
if generation=1950               then duree_req=162 ;
if generation=1951               then duree_req=163 ;
if generation=1952               then duree_req=164 ;

* distDA;
dist_DA = valid_tot-duree_req;
* groups;
group_1=0;
if (ID601>=duree_req)						 then  group_1=1;  	*FR reached at 60;
if (duree_req-20<=ID601 & ID601<duree_req)   then  group_1=2;   *FR reached bw 60 and 65;	
if (ID601 <duree_req-20)                     then  group_1=3;   *FR reached after 65;
run;
endrsubmit;



/*** I.2 Merge with employers information ***/
/** I.2.a Merge datasets */ 
rsubmit;
%macro filters3();
data work.temp2;
set xtrav.ech2014_enreg6_hades (keep=    ident  %do i=2000 %to 2014;   cd_ape_etab_&i._1 cd_ape_etab_&i._2  %end ; );
run;
%mend;      
endrsubmit;
rsubmit;
%filters3();
endrsubmit;

rsubmit;
data base;
merge temp  (in=main)
	  temp2 (in=b);
by ident; 
if main;
run;
endrsubmit;




/*** I.3 Dummy on derogatory agreement (list established in 2006) ***/
data xwork.accord_naf;
infile 'O:\bunching\MRO\NAF_MRO.txt' delimiter='' MISSOVER DSD lrecl=32767 firstobs=2;
informat id $4. ;
informat cd_naf_700 $6. ;
format id  $4. ;
format cd_naf_700 $6. ;
input id cd_naf_700 ;
run;

/* Dummy on agreement for each individual and each year */
rsubmit;
%macro ind_agreement();
proc sort data=accord_naf; by cd_naf_700 ; run; 
%do a=2000 %to 2014;
proc sort data=temp; by cd_ape_etab_&a._1 ; run; 
data temp; merge temp (in=main2) accord_naf (in=b rename=(cd_naf_700=cd_ape_etab_&a._1));
by cd_ape_etab_&a._1; 
if main2;
if main2 & b then ind_&a.=1; else ind_&a.=0;
run;
%end ;
%mend;      
endrsubmit;
rsubmit;
data temp;
set base;
%ind_agreement();
endrsubmit;



/* Ind for last employer and employer at 60 mro */
rsubmit;
%macro var();
ind_last=0;
ind_60  =0;
length naf_60 $5;
length naf_last $5;
%do a=2000 %to 2014;  
    if an_60=&a. and ind_&a.=1 then do ; 
	ind_60   =1;   
	naf_60=cd_ape_etab_&a._1 ; 
	end;
	if an_lastw=&a. and ind_&a.=1 then do ; 
	ind_last = 1;   
	naf_last = cd_ape_etab_&a._1 ; 
	end;
%end ;
run;
%mend;      
data temp;
set  temp;
%var();
endrsubmit;


rsubmit;
proc freq data=temp;
table ind_60;
run;
endrsubmit;

rsubmit;
proc freq data=temp;
table ind_last;
run;
endrsubmit;


rsubmit;
data xtrav.temp;
set temp;
run;
endrsubmit;

/**** Save output ****/
/** One line by individual **/
libname O 'O:\bunching\MRO\programmes\';
data O.data;
 set trav.temp (keep=    ident sexe nb_enf generation trim_nais mois_nais  prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali ID601 dist_DA
                          ageliq_month ageliq_trim2 an_lastw an_60 ind_last ind_60 naf_60 naf_last group_1); 
run;


/** Wide: one line by year **/
* variable selection;
rsubmit;
%macro filters4();
data temp;
set  xtrav.temp (keep=    ident sexe nb_enf generation trim_nais mois_nais  prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali ID601 dist_DA
                          ageliq_month ageliq_trim2 an_lastw an_60 ind_last ind_60 naf_60 naf_last group_1
       						%do i=2000 %to 2014;   ind_&i.  %end ; );
if (an_ej>=2000 or an_lastw>=2000 or an_ej=.);
run;
%mend;      
endrsubmit;
rsubmit;
%filters4();
endrsubmit;

rsubmit;
data wide;
set  temp (keep= ident ind_2000 ind_2001 ind_2002 ind_2003 ind_2004 ind_2005 ind_2006 
				 	   ind_2007 ind_2008 ind_2009 ind_2010 ind_2011 ind_2012 ind_2013 ind_2014 );
run;
proc sort data=work.wide ; by ident  ; 
run;
data indiv;
set  temp (keep=    ident sexe nb_enf generation trim_nais mois_nais  prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali ID601 dist_DA
                          ageliq_month ageliq_trim2 an_lastw an_60 ind_last ind_60 naf_60 naf_last group_1);
run;
endrsubmit;

* long to wide;
rsubmit;
proc transpose data=work.wide  out=work.long;
by ident ;
run;
data long;
 set long (rename=(col1=ind_mro));
 year=input(substr(_name_, 5), 5.);
 drop _name_;
run; 
endrsubmit;
* individual variables;
rsubmit;
proc sort data=work.long ; by ident  ; 
proc sort data=work.indiv ; by ident  ; 
data base;
merge long  (in=main)
	  indiv (in=b);
by ident; 
if main;
run;
endrsubmit;

/* Variables: dummies for, retire a given year, claim pension a given year */
rsubmit; 
data base;
set base;
t_liq   = 0;
  liq   = 0;
t_finact= 0;
  finact= 0;
if an_ej=year    then t_liq=1;
if an_ej<=year   then   liq=1;
if an_lastw=year then t_finact=1;
if an_lastw<=year then   finact=1;
run;
endrsubmit;

/* Selection: group2, generation 1938-1947 retired*/
rsubmit; 
data work.base;
set  work.base; 
if group_1=2;
if generation<=1947;
if generation>=1938;
if an_ej^=.;
run;
endrsubmit; 

libname O 'O:\bunching\MRO\programmes\';
data O.data_long;
set  xwork.base; 
run;


