/* ------------------------------------------------------------------------------------ */
/* extraits du prog sas sous \\SHARED\donnees\DIR70\Dep130\Mise � la retraite d'office
   "Mise � la retraite d'office - extraction fichiers bruts - cotisants 2004.sas" */
/* ------------------------------------------------------------------------------------ */
/* date de maj: 24/07/2007 																*/
/* ------------------------------------------------------------------------------------ */


/* 3. prestataires cotisant en 2004 
	  avec taux plein et �ge entre 60 et 65 ans */

/* sans accord */
rsubmit;
title "sans accord";
proc sql;
select sexe, age_2004,
round(count(nir)*19.4,1) as nb_nir
from xarmiret.cotisants_04_ape_siret_accord
where accord = ''
and an_ej=2004
and 60 <= age_2004 <= 65
and taux=0.5
group by sexe, age_2004; 	
quit;
endrsubmit;

/* avec accord */
rsubmit;
title "avec accord";
proc sql;
select sexe, age_2004,
round(count(nir)*19.4,1) as nb_nir
from xarmiret.cotisants_04_ape_siret_accord
where accord ne ''
and an_ej=2004
and 60 <= age_2004 <= 65
and taux=0.5
group by sexe, age_2004; 	
quit;
endrsubmit;
