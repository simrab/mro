/***************************************************************************************************************************************************************************
*
*  MRO: first part of the paper: before/after 2003
* - for all
* - only without derogatory agreements. 
* => Evolution of employment rate by age category (55-59,60-64,65+) by year.
* GOAL: reproduce Claude Mini (Dares, 2013)
*  Sample restriction? Employed in RG at 50. Not only retired. 


/***** 0. Initialisation  ********/

/** Connexion **/
%let srv=SasSrvConnect.n18.an.cnav 7551;
option comamid=tcp remote=srv;
signon noscript user=_prompt_ password=_prompt_;

rsubmit;
libname xtrav '/travail8/z078886';
endrsubmit;
libname trav slibref=xtrav server=srv;
libname xwork slibref=work server=srv;

%INCLUDE "O:\bunching\Echant20e_2014\formats.sas";

/***** I. Work on data ********/



** I.1.a filters;
/* Filters 1:  Variables + generations */
rsubmit;
%macro filters1();
data work.temp;
set xtrav.Table_init_ech14_20e_avtc_eval (keep=    ident sexe nb_enf generation trim_nais mois_nais an_deces prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali categ
       %do i=1947 %to 2014;  rep_&i._1--tr_&i._4  plaf_&i.--avpf_&i.  %end ; );
if generation>=1915;
if generation<=1958;
run;
%mend;      
endrsubmit;
rsubmit;
%filters1();
endrsubmit;

/* Filters 2: an_fin act + dummy for actity*/
rsubmit;
data work.temp2;
set  work.temp; 
/* Arrays */
array ind_emp (1947:2014,1)  ind_emp_1947-ind_emp_2014 ;
array sal(1947:2014,4)   plaf_1947--avpf_2014  ;
array rep(1947:2014,2,4) rep_1947_1--tr_2014_4 ;
* Variables;
an_finact   =0;  * year of last report;
age_finact  =0;  * age of last report;
last_rep    =''; * type of last report;
an_60       =0;  * year of 60;
do a = 1965 to 2014; * Loop on years;
age=a-generation;
* Type of last report before liq;
if  (rep(a,1,1)='e' or rep(a,1,1)='h' or rep(a,1,1)='k')  then do;
	last_rep = rep(a,1,1);
	an_finact=a; 
	age_finact=age;  
    ind_emp(a,1) = 1;
end;
if sal(a,1)>0  then do;
	an_finact=a; 
	age_finact=age;  
	ind_emp(a,1) = 1;
end;
end;
run;
endrsubmit;


* Filters:  ;
rsubmit;
data work.table_init1;
set  work.temp2 ;
run;
data work.table_init2;
set  work.temp2 ;
if age_finact>=50;
run;
data work.table_init3;
set  work.temp2 ;
if age_finact>=59;
run;
endrsubmit;



/***** II. Employment rate by age and date********/
rsubmit;
%macro employ_rate(database);
* Loop on years;
%do a=1985 %to 2013;
proc sql;create table emp_rate_&a. as
select sexe,
sum(ind_emp_&a*(55=&a.-generation))/sum(55=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_55_&a.,
sum(ind_emp_&a*(56=&a.-generation))/sum(56=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_56_&a.,
sum(ind_emp_&a*(57=&a.-generation))/sum(57=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_57_&a.,
sum(ind_emp_&a*(58=&a.-generation))/sum(58=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_58_&a.,
sum(ind_emp_&a*(59=&a.-generation))/sum(59=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_59_&a.,
sum(ind_emp_&a*(60=&a.-generation))/sum(60=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_60_&a.,
sum(ind_emp_&a*(61=&a.-generation))/sum(61=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_61_&a.,
sum(ind_emp_&a*(62=&a.-generation))/sum(62=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_62_&a.,
sum(ind_emp_&a*(63=&a.-generation))/sum(63=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_63_&a.,
sum(ind_emp_&a*(64=&a.-generation))/sum(64=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_64_&a.,
sum(ind_emp_&a*(65=&a.-generation))/sum(65=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_65_&a.,
sum(ind_emp_&a*(66=&a.-generation))/sum(66=&a.-generation and (&a.<an_deces or an_deces=.)) as emp_rate_66_&a.,

sum(ind_emp_&a*(50<=&a.-generation<55))/sum(50<=&a.-generation<55 and (&a.<an_deces or an_deces=.)) as emp_rate_A1_&a.,
sum(ind_emp_&a*(55<=&a.-generation<60))/sum(55<=&a.-generation<60 and (&a.<an_deces or an_deces=.)) as emp_rate_A2_&a.,
sum(ind_emp_&a*(50<=&a.-generation<60))/sum(50<=&a.-generation<60 and (&a.<an_deces or an_deces=.)) as emp_rate_A1A2_&a.,
sum(ind_emp_&a*(60<=&a.-generation<65))/sum(60<=&a.-generation<65 and (&a.<an_deces or an_deces=.)) as emp_rate_A3_&a.,
sum(ind_emp_&a*(65<=&a.-generation<70))/sum(65<=&a.-generation<70 and (&a.<an_deces or an_deces=.)) as emp_rate_A4_&a.
from &database.
group by sexe;
quit;
%end;
data emp_rate;
merge emp_rate_1985 emp_rate_1986 emp_rate_1987 emp_rate_1988 emp_rate_1989 
	  emp_rate_1990 emp_rate_1991 emp_rate_1992 emp_rate_1993 emp_rate_1994 emp_rate_1995 emp_rate_1996 emp_rate_1997 emp_rate_1998 emp_rate_1999
	  emp_rate_2000 emp_rate_2001 emp_rate_2002 emp_rate_2003 emp_rate_2004 emp_rate_2005 emp_rate_2006 emp_rate_2007 emp_rate_2008 emp_rate_2009
	  emp_rate_2010 emp_rate_2011 emp_rate_2012 ;
by sexe;
run;
proc transpose data=work.emp_rate  out=work.emp_rate_A1;
var emp_rate_A1_1985-emp_rate_A1_2012 ;
by sexe;
run; 
data emp_rate_A1;
 set emp_rate_A1 (rename=(col1=y50_54));
 year=input(substr(_name_, 13), 5.);
 drop _name_;
run;
proc transpose data=work.emp_rate  out=work.emp_rate_A2;
var emp_rate_A2_1985-emp_rate_A2_2012 ;
by sexe;
run; 
data emp_rate_A2;
 set emp_rate_A2 (rename=(col1=y55_59));
 year=input(substr(_name_, 13), 5.);
 drop _name_;
run;
proc transpose data=work.emp_rate  out=work.emp_rate_A1A2;
var emp_rate_A1A2_1985-emp_rate_A1A2_2012 ;
by sexe;
run; 
data emp_rate_A1A2;
 set emp_rate_A1A2 (rename=(col1=y50_59));
 year=input(substr(_name_, 15), 5.);
 drop _name_;
run;
proc transpose data=work.emp_rate  out=work.emp_rate_A3;
var emp_rate_A3_1985-emp_rate_A3_2012 ;
by sexe;
run; 
data emp_rate_A3;
 set emp_rate_A3 (rename=(col1=y60_64));
 year=input(substr(_name_, 13), 5.);
 drop _name_;
run;
proc transpose data=work.emp_rate  out=work.emp_rate_A4;
var emp_rate_A4_1985-emp_rate_A4_2012 ;
by sexe;
run; 
data emp_rate_A4;
 set emp_rate_A4 (rename=(col1=y65_69));
 year=input(substr(_name_, 13), 5.);
 drop _name_;
run;
data emp_rate_all_1;
merge emp_rate_A1 emp_rate_A2 emp_rate_A1A2 emp_rate_A3 emp_rate_A4;
by sexe;
run;
%do a=55 %to 66;
proc transpose data=work.emp_rate  out=work.emp_rate_&a.;
var emp_rate_&a._1985-emp_rate_&a._2012 ;
by sexe;
run;
data emp_rate_&a;
 set emp_rate_&a (rename=(col1=age_&a.));
 year=input(substr(_name_, 13), 5.);
 drop _name_;
run; 
%end;
data emp_rate_all_2;
merge emp_rate_55 emp_rate_56 emp_rate_57 emp_rate_58 emp_rate_59 
	  emp_rate_60 emp_rate_61 emp_rate_62 emp_rate_63 emp_rate_64 emp_rate_65 emp_rate_66;
by sexe;
run;
%mend;
endrsubmit;

* All;
rsubmit;
%employ_rate(table_init1);
endrsubmit;
libname O 'O:\bunching\MRO\data\output\';
data O.emp_rate1;
 set xwork.emp_rate_all_1; 
run;
data O.emp_rate_1_age;
 set xwork.emp_rate_all_2; 
run;
rsubmit;
%employ_rate(table_init2);
endrsubmit;
libname O 'O:\bunching\MRO\data\output\';
data O.emp_rate2;
 set xwork.emp_rate_all_1; 
run;
data O.emp_rate_2_age;
 set xwork.emp_rate_all_2; 
run;
rsubmit;
%employ_rate(table_init3);
endrsubmit;
libname O 'O:\bunching\MRO\data\output\';
data O.emp_rate3;
 set xwork.emp_rate_all_1; 
run;
data O.emp_rate_3_age;
 set xwork.emp_rate_all_2; 
run;


/***** III. Effect of ********/



