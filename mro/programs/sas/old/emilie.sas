/************************************************/
/* 			Mise � la retraite d'office			*/
/*												*/
/*		(derni�re maj du prg : le 23/10/06)		*/
/************************************************/


/* ---------------------------------------------------------------- */
/*		Extraction du code APE et du SIRET des fichiers bruts		*/
/* ---------------------------------------------------------------- */

rsubmit;
filename g00a09 '/bases/echant2006/CRAM012.L11B25CA.g1900a1909';
filename g10a19 '/bases/echant2006/CRAM012.L11B25CA.g1910a1919';
filename g20a29 '/bases/echant2006/CRAM012.L11B25CA.g1920a1929';
filename g30a39 '/bases/echant2006/CRAM012.L11B25CA.g1930a1939';
filename g40a49 '/bases/echant2006/CRAM012.L11B25CA.g1940a1949';
filename g50a59 '/bases/echant2006/CRAM012.L11B25CA.g1950a1959';
filename g60a64 '/bases/echant2006/CRAM012.L11B25CA.g1960a1964';
filename g65a69 '/bases/echant2006/CRAM012.L11B25CA.g1965a1969';
filename g70a74 '/bases/echant2006/CRAM012.L11B25CA.g1970a1974';
filename g75a79 '/bases/echant2006/CRAM012.L11B25CA.g1975a1979';
filename g80a84 '/bases/echant2006/CRAM012.L11B25CA.g1980a1984';
filename g85a89 '/bases/echant2006/CRAM012.L11B25CA.g1985a1989';
filename g90a94 '/bases/echant2006/CRAM012.L11B25CA.g1990a1994';
filename g95a05 '/bases/echant2006/CRAM012.L11B25CA.g1995a2005';
endrsubmit;

/************************************************************************/
/* Macro enr22 permettant d'extraire le code APE de l'enregistrement 22 */
/************************************************************************/

rsubmit;
%macro enr22(fich=);

/* lecture de l'enregistrement 22 */
data enr22_&fich.;

infile &fich. missover recfm=v lrecl=5154;
input @22 typ_enreg $2. @;

if typ_enreg='22' then do;
	input 	@1	nir $21.
			@27	nb_occ_enreg 3.
			@30 (dt_deb_1-dt_deb_62) ($8. +71)
			@38 (dt_fin_1-dt_fin_62) ($8. +71)
			@59 (ty_id_emp_1-ty_id_emp_62) ($1. +78)
			@60 (nu_emp_1-nu_emp_62) ($18. +61)
			@78 (cd_act_emp_1-cd_act_emp_62) ($4. +75)
			@82 (cd_lieu_rsd_1-cd_lieu_rsd_62) ($3. +76)
			@85 (cd_lieu_act_1-cd_lieu_act_62) ($3. +76);
	output;
end;

run;

/* s�lection du code APE le plus r�cent et du code APE de 2004 */
data enr22_&fich.;
set enr22_&fich.;

array dt_deb_enr22(62) $8 dt_deb_1-dt_deb_62;
array dt_fin_enr22(62) $8 dt_fin_1-dt_fin_62;
array cd_act_emp_enr22(62) $4 cd_act_emp_1-cd_act_emp_62;
length vali_max $4.;

/* initialisation des valeurs */
dt_fin_max = dt_fin_enr22(1);
cd_ape_max = cd_act_emp_enr22(1);

do i=1 to nb_occ_enreg;
	if (dt_fin_enr22(i) = '' and cd_act_emp_enr22(i) ne '') then do;
		/* r�cup�ration ann�e 2004 ou 2005 */
		dt_fin_max = '99999999';
		cd_ape_max = cd_act_emp_enr22(i);
	end;
	if dt_fin_enr22(i) ne '' and (dt_fin_enr22(i) >= dt_fin_max) then do;
		dt_fin_max = dt_fin_enr22(i);
		cd_ape_max = cd_act_emp_enr22(i);
	end;
end;

vali_max = substr(dt_fin_max,1,4);
if vali_max in ('2004','2005') then cd_ape_2004 = cd_ape_max;

drop i;
run;



/* tri de la table par nir et validit� max */
proc sort data = enr22_&fich.; by nir vali_max; run;


/* s�lection de la derni�re ligne pour chaque nir ==> �liminer les doublons */
data enr22_vali_max_&fich. (keep = nir vali_max cd_ape_max);
set enr22_&fich.; by nir;
if last.nir then output;
run;


/* s�lection de la ligne avec une validit� en 2004 */
data enr22_vali_2004_&fich. (keep = nir cd_ape_2004);
set enr22_&fich.; by nir;
if cd_ape_2004 ne '' and last.nir then output;
run;


/* concat�nation des 2 tables pr�c�dentes */
data enr22_ape_&fich.;
merge enr22_vali_max_&fich. (in=a)
	  enr22_vali_2004_&fich. ;
by nir; if a;
run;

%mend enr22;
endrsubmit;

/* Ex�cution de la macro sur les 14 fichiers bruts de l'�chantillon au 20�me */
rsubmit;
%enr22(fich=g00a09);
endrsubmit;

rsubmit;
%enr22(fich=g10a19);
endrsubmit;

rsubmit;
%enr22(fich=g20a29);
endrsubmit;

rsubmit;
%enr22(fich=g30a39);
endrsubmit;

rsubmit;
%enr22(fich=g40a49);
endrsubmit;

rsubmit;
%enr22(fich=g50a59);
endrsubmit;

rsubmit;
%enr22(fich=g60a64);
endrsubmit;

rsubmit;
%enr22(fich=g65a69);
endrsubmit;

rsubmit;
%enr22(fich=g70a74);
endrsubmit;

rsubmit;
%enr22(fich=g75a79);
endrsubmit;

rsubmit;
%enr22(fich=g80a84);
endrsubmit;

rsubmit;
%enr22(fich=g85a89);
endrsubmit;

rsubmit;
%enr22(fich=g90a94);
endrsubmit;

rsubmit;
%enr22(fich=g95a05);
endrsubmit;

/* concat�nation des enregistrements */
rsubmit;
data xmiseret.ech2006_enr22_ape;
	set enr22_ape_g00a09 
 		enr22_ape_g10a19 
		enr22_ape_g20a29 
 		enr22_ape_g30a39 
		enr22_ape_g40a49 
		enr22_ape_g50a59 
 		enr22_ape_g60a64 
 		enr22_ape_g65a69 
 		enr22_ape_g70a74 
 		enr22_ape_g75a79 
 		enr22_ape_g80a84 
 		enr22_ape_g85a89 
 		enr22_ape_g90a94
 		enr22_ape_g95a05;
run;
proc sort data = xmiseret.ech2006_enr22_ape; by nir; run;
endrsubmit;
/* 3 183 413 obs */


/* recherche de doublons ==> aucun doublon */
rsubmit;
proc sql;
select count (distinct nir) as nb_nir
from xmiseret.ech2006_enr22_ape;
quit;
endrsubmit;

/* d�nombrement */
rsubmit;
proc sql;
select count (nir) as nb_nir
from xmiseret.ech2006_enr22_ape
where cd_ape_2004 ne '';
quit;
endrsubmit;
/* nombre de code activit� renseign�s en 2004 ==> 349 920 soit 25 % de l'enr 22 */


/*********************************************************************/
/* Macro enr24 permettant d'extraire le SIRET de l'enregistrement 24 */
/*********************************************************************/

rsubmit;
%macro enr24(fich=);

/* lecture de l'enregistrement 24 */
data enr24_&fich.;

infile &fich. missover recfm=v lrecl=5154;
input @22 typ_enreg $2. @;

if typ_enreg='24' then do;
	input 	@1	nir $21.
			@27	nb_occ_enreg 3.
			@30 (vali_vrsm_1-vali_vrsm_61) ($5.+75)
			@47 (ty_id_emp_1-ty_id_emp_61) ($1.+79)
			@48 (nu_emp_1-nu_emp_61) ($18.+62)
			@66	(cd_cdt_emp_1-cd_cdt_emp_61) ($1.+79)
			@68 (cd_emp_1-cd_emp_61) ($4.+76)
			@72 (cd_trv_etg_1-cd_trv_etg_61) ($1.+79);
	output;
end;

run;


/* s�lection du dernier num�ro d'employeur connu (nu_emp_max) avec l'ann�e de validit� associ�e
et du num�ro d'employeur en 2004 s'il existe (nu_emp_2004) */
data enr24_&fich. (keep = nir nb_occ_enreg vali_max ty_id_emp_max nu_emp_max cd_cdt_emp_max
						  cd_emp_max cd_trv_etg_max
						  ty_id_emp_2004 nu_emp_2004 cd_cdt_emp_2004
						  cd_emp_2004 cd_trv_etg_2004);
set enr24_&fich.;

array vali_vrsm_enr24(61) $5 vali_vrsm_1-vali_vrsm_61;
array ty_id_emp_enr24(61) $1 ty_id_emp_1-ty_id_emp_61;
array nu_emp_enr24(61) $18 nu_emp_1-nu_emp_61;
array cd_cdt_emp_enr24(61) $1 cd_cdt_emp_1-cd_cdt_emp_61;
array cd_emp_enr24(61) $4 cd_emp_1-cd_emp_61;
array cd_trv_etg_enr24(61) $1 cd_trv_etg_1-cd_trv_etg_61;

/* initialisation des valeurs */
vali_max = vali_vrsm_enr24(1);
ty_id_emp_max = ty_id_emp_enr24(1);
nu_emp_max = nu_emp_enr24(1);
cd_cdt_emp_max = cd_cdt_emp_enr24(1);
cd_emp_max = cd_emp_enr24(1);
cd_trv_etg_max = cd_trv_etg_enr24(1);

do i=1 to nb_occ_enreg;
	if vali_vrsm_enr24(i) >= vali_max then do;
		vali_max = vali_vrsm_enr24(i);
		ty_id_emp_max = ty_id_emp_enr24(i);
		nu_emp_max = nu_emp_enr24(i);
		cd_cdt_emp_max = cd_cdt_emp_enr24(i);
		cd_emp_max = cd_emp_enr24(i);
		cd_trv_etg_max = cd_trv_etg_enr24(i);
	end;
	if substr(vali_vrsm_enr24(i),1,4) in ('2004','2005') then do;
		ty_id_emp_2004 = ty_id_emp_enr24(i);
		nu_emp_2004 = nu_emp_enr24(i);
		cd_cdt_emp_2004 = cd_cdt_emp_enr24(i);
		cd_emp_2004 = cd_emp_enr24(i);
		cd_trv_etg_2004 = cd_trv_etg_enr24(i);
	end;
end;

drop i;
run;


/* tri de la table par nir et validit� max */
proc sort data = enr24_&fich.; by nir vali_max; run;


/* s�lection de la derni�re ligne pour chaque nir ==> �liminer les doublons */
data enr24_vali_max_&fich. (keep = nir vali_max ty_id_emp_max nu_emp_max) ;
set enr24_&fich.; by nir;
if last.nir then output;
run;


/* s�lection de la ligne avec une validit� en 2004 */
data enr24_vali_2004_&fich. (keep = nir ty_id_emp_2004 nu_emp_2004) ;
set enr24_&fich.; by nir;
if ty_id_emp_2004 ne '' and last.nir then output;
run;


/* concat�nation des 2 tables pr�c�dentes */
data enr24_vali_emp_&fich. (keep =  nir vali_max ty_id_emp_max nu_emp_max 
									ty_id_emp_2004 nu_emp_2004);
merge enr24_vali_max_&fich. (in=a)
	  enr24_vali_2004_&fich. ;
by nir; if a;
run;

%mend enr24;
endrsubmit;


/* Ex�cution de la macro sur les 14 fichiers bruts de l'�chantillon au 20�me */
rsubmit;
%enr24(fich=g00a09);
endrsubmit;

rsubmit;
%enr24(fich=g10a19);
endrsubmit;

rsubmit;
%enr24(fich=g20a29);
endrsubmit;

rsubmit;
%enr24(fich=g30a39);
endrsubmit;

rsubmit;
%enr24(fich=g40a49);
endrsubmit;

rsubmit;
%enr24(fich=g50a59);
endrsubmit;

rsubmit;
%enr24(fich=g60a64);
endrsubmit;

rsubmit;
%enr24(fich=g65a69);
endrsubmit;

rsubmit;
%enr24(fich=g70a74);
endrsubmit;

rsubmit;
%enr24(fich=g75a79);
endrsubmit;

rsubmit;
%enr24(fich=g80a84);
endrsubmit;

rsubmit;
%enr24(fich=g85a89);
endrsubmit;

rsubmit;
%enr24(fich=g90a94);
endrsubmit;

rsubmit;
%enr24(fich=g95a05);
endrsubmit;

/* concat�nation des enregistrements */
rsubmit;
data xmiseret.ech2006_enr24_siret;
	set enr24_vali_emp_g00a09 
 		enr24_vali_emp_g10a19 
		enr24_vali_emp_g20a29 
 		enr24_vali_emp_g30a39 
		enr24_vali_emp_g40a49 
		enr24_vali_emp_g50a59 
 		enr24_vali_emp_g60a64 
 		enr24_vali_emp_g65a69 
 		enr24_vali_emp_g70a74 
 		enr24_vali_emp_g75a79 
 		enr24_vali_emp_g80a84 
 		enr24_vali_emp_g85a89 
 		enr24_vali_emp_g90a94 
 		enr24_vali_emp_g95a05 ;
run;
proc sort data = xmiseret.ech2006_enr24_siret; by nir; run;
endrsubmit;
/* 2 685 451 obs */


/* recherche de doublons ==> aucun doublon */
rsubmit;
proc sql;
select count (distinct nir) as nb_nir
from xmiseret.ech2006_enr24_siret;
quit;
endrsubmit;

/* d�nombrement */
rsubmit;
proc sql;
select count (nir) as nb_nir
from xmiseret.ech2006_enr24_siret
where ty_id_emp_2004 = '2';
quit;
endrsubmit;
/* nombre de SIRET renseign�s en 2004 ==> 1 133 556 soit 42 % de l'enr 24 */


/****************************************************/
/* Cr�ation de la table finale en concat�nant
	- les assur�s de table initiale
	- cotisants en 2003 et 2004
	- avec les donn�es des fichiers bruts pour avoir
	- les codes siret et APE des fichiers bruts	
	- insertion du code NAF 700 de l'Insee			*/
/****************************************************/

/* Cotisants en 2003 et 2004 */
rsubmit;
data xmiseret.cotisants_03_04;
set xmodele.table_init_ech06_20e_majprestadc;
if (plaf_2003 > 0 or plaf_2004 > 0) then output;
run;
endrsubmit;

/* Cr�ation de la table naf 700 r�cup�r�e sur le site de l'Insee */
data workx.naf_700;
infile 'D:\Donn�es\mise retraite doffice\nomenclature NAF INSEE\naf_700.txt' delimiter='09'x MISSOVER DSD lrecl=32767 firstobs=2;
informat cd_naf_700 $4. ;
informat Lib_naf_700 $200. ;
format cd_naf_700 $4. ;
format Lib_naf_700 $200. ;

input
cd_naf_700 $
Lib_naf_700 $
;
run;
rsubmit;
data xmiseret.naf_700;
set naf_700;
run;
endrsubmit;

/* cr�ation TABLE DE BASE pour MISE RETRAITE D'OFFICE */
rsubmit;
data xmiseret.cotisants_03_04_ape_siret;
merge xmiseret.cotisants_03_04 (in=a)
	  xmiseret.ech2006_enr22_ape (rename=(vali_max=vali_max_ape))
	  xmiseret.ech2006_enr24_siret (rename=(vali_max=vali_max_siret));
by nir; if a;

/* �ge r�volu en fin d'ann�e */
age_2004 = 2004 - generation;

if input(vali_max_ape,4.) >= 2003 then do;
	cd_naf_220 = substr(cd_ape_max,1,3);
	cd_naf_700 = cd_ape_max;
end;

if input(vali_max_siret,4.) >= 2003 and ty_id_emp_max = '2'
then siret = substr(nu_emp_max,5,14);

run;
endrsubmit;

/* tri de la table par cd_naf_700 */
rsubmit;
proc sort data = xmiseret.cotisants_03_04_ape_siret; by cd_naf_700;run;
endrsubmit;

/* merge sur cd_naf_700 */
rsubmit;
data xmiseret.cotisants_03_04_ape_siret;
merge xmiseret.cotisants_03_04_ape_siret (in=a)
	  xmiseret.naf_700;
by cd_naf_700; if a;
run;
endrsubmit;

rsubmit;
proc sort data = xmiseret.cotisants_03_04_ape_siret; by nir;run;
endrsubmit;

/* ==> table de r�f�rence pour l'�tude:  COTISANTS_03_04_APE_SIRET    1 275 570 obs */


/****************/
/* Statistiques */
/****************/

/* d�nombrement des prestataires */
rsubmit;
proc freq data = xmiseret.cotisants_03_04_ape_siret;
tables an_ej/missing;
run;
endrsubmit;
/* 1 236 949 cotisants (97%) 
      38 621 prestataires (3%) entre 1967 et 2006 avec maximum des effectifs en
		2004 = 11 938 presta (1%)
		2005 = 12 128 presta (1%) 
        soit environ 10 000 presta avant 2004 */

/* ---> conservation des assur�s non prestataires ou prestataires apr�s 2004 */



/* -- CODE et LIBELLE NAF -- */

/* d�nombrement */
rsubmit;
proc sql;
select count(nir) as nb_nir
from xmiseret.cotisants_03_04_ape_siret
where cd_naf_700 ne '' and lib_naf_700 = ''; /* ==> 24 781 au lieu de 18 464 pour naf 220 */
/*where cd_naf_700 = '' and lib_naf_700 = ''; /* ==> 908 313 */
/*where cd_naf_700 ne '' and lib_naf_700 ne ''; /* ==> 342 476 au lieu de 348 793 pour naf 220 */
/*where cd_naf_700 = '' and lib_naf_700 ne ''; /* ==> 0 */;
quit;
endrsubmit;
/* 1 275 570 */

/* verification : 
   si le code NAF est connu mais n'est associ� � aucun libell� -> Code NAF 700 aberrant */
rsubmit;
proc sql;
create table test as
select cd_naf_700
from xmiseret.cotisants_03_04_ape_siret
where cd_naf_700 ne '' and lib_naf_700 = '';
quit;
endrsubmit;

/* ---> Conservation des assur�s avec code et libell� NAF */

/* liste des naf quand un libell� est connu */
rsubmit;
proc sql;
create table xmiseret.cotisants_03_04_ape_ok as
select *
from xmiseret.cotisants_03_04_ape_siret
where cd_naf_700 ne '' and lib_naf_700 ne '' ;
quit;
endrsubmit;
/* 342 476 soit 27 % des cotisants 2003/2004*/

rsubmit;
proc freq data = xmiseret.cotisants_03_04_ape_ok;
tables lib_naf_700/ out=liste_naf_700 ;
run;
endrsubmit;

proc export 
data = workx.liste_naf_700
outfile = 'd:\temp\liste_naf_700.txt'  replace;
run;

/* r�partition par �ge */
rsubmit;
proc freq data = xmiseret.cotisants_03_04_ape_ok;
tables age_2004 ;
where an_ej >2004 or an_ej=.;
run;
endrsubmit;
/* 331 141 obs */

/* ---> comme les d�rogations de mise � la retraite d'office concernent 
	- des assur�s < 65 ans et potentiellement en RA >= 56 ans
	- jusqu'� fin 2009
   il convient de restreindre l'analyse aux cotisants en 2004 
   �g�s entre 51 et 66 ans (�ge r�volu en fin d'ann�e) */

rsubmit;
proc format;
	value age 
	0-50='0-50 ans'
	51-59='51-59 ans'
	60-66='60-66 ans'
	67-high='>67 ans';
run;
endrsubmit;
rsubmit;
proc freq data = xmiseret.cotisants_03_04_ape_ok;
tables age_2004 ;
where an_ej >2004 or an_ej=.;
format age_2004 age.;
run;
endrsubmit;
/*                                              Cumulative    Cumulative
           age_2004    Frequency     Percent     Frequency      Percent
          0-50 ans       276752       83.58        276752        83.58
          51-59 ans       48499       14.65        325251        98.22
          60-66 ans        5392        1.63        330643        99.85
          >67 ans           498        0.15        331141       100.00
*/




/* -- CODE NAF et SIRET -- */

/* d�nombrement sans filtre */
rsubmit;
proc sql;
select count(nir) as nb_nir
from xmiseret.cotisants_03_04_ape_siret
/*where cd_naf_700 ne '' and siret = ''; 	/* ==> 2 678 */
/*where cd_naf_700 = '' and siret = ''; 	/* ==> 47 625 */
/*where cd_naf_700 ne '' and siret ne ''; 	/* ==> 364 579 */
/*where cd_naf_700 = '' and siret ne ''; 	/* ==> 860 688 */
where siret ne ''							/* ==> 1 225 267 obs */
;
quit;
endrsubmit;




/*********************************************************************/
/* Enrichissement de la table de r�f�rence cotisants_03_04_ape_siret
   avec le code APE correspondant au n� Siret donn� par Tours 	     */
/*********************************************************************/

/*  -- cr�ation de la table avec siret sans code ape pour demande info � Tours -- */
rsubmit;
proc sql;
create table chr_siret as
select distinct(siret)
from xmiseret.cotisants_03_04_ape_siret
where siret ne ''; 
quit;
endrsubmit;
/* ==>  477 557 sur 1 225 267 obs */
proc export 
data = workx.chr_siret
outfile = 'd:\temp\chr_siret.txt'  replace;
run;


/* -- r�cup�ration des n� Siret sur 14 caract�res + code APE sur 4 caract�res de Tours -- */
data workx.chr_siret_ape;
infile 'D:\Donn�es\mise retraite doffice\25oct2006-CA-chr_siret_ape.txt' delimiter='09'x MISSOVER DSD lrecl=32767 firstobs=2;
informat cd_siret_ape $18. ;
format cd_siret_ape $18. ;
input cd_siret_ape $ ;
run;
rsubmit;
data xmiseret.chr_siret_ape;
set chr_siret_ape;
run;
endrsubmit;
/* 441 876 obs sur 477 557 soit 92,5% */
rsubmit;
data xmiseret.chr_siret_ape;
set xmiseret.chr_siret_ape;
siret=substr(cd_siret_ape,1,14);
cd_naf_700=substr(cd_siret_ape,15,4);
drop cd_siret_ape;
run;
endrsubmit;
/* verif */
rsubmit;
proc sql;
select count(distinct(siret))
from xmiseret.chr_siret_ape
where siret ne ''; 
quit;
endrsubmit;
/* ==>  441 876 obs ==> aucun doublon dans le fichier de Tours */

/* -- r�cup�ration du code NAF correspondant ==> merge avec table naf_700 */
rsubmit;
proc sort data=xmiseret.chr_siret_ape out=chr_siret_ape; by cd_naf_700 ; run;
endrsubmit;
rsubmit;
data chr_siret_ape;
merge chr_siret_ape (in=a)
	  xmiseret.naf_700;
by cd_naf_700; if a;
if lib_naf_700 = '' then delete;
run;
endrsubmit;
/* 441 774 obs sur 441 876 au total ==> 99.97% de sirets valides */
rsubmit;
proc sort data=chr_siret_ape out=xmiseret.chr_siret_ape; by siret ; run;
endrsubmit;

/* -- affectation d'un code APE pour chaque Siret qui est associ� � plusieurs nir -- */
rsubmit; 
data nir_siret;
set xmiseret.cotisants_03_04_ape_siret (keep=nir siret); 
run;
endrsubmit;
rsubmit; 
proc sort data=nir_siret ; 
by siret;  run;
endrsubmit;
rsubmit;
data nir_siret_ape ;
merge nir_siret (in=a)
	  xmiseret.chr_siret_ape;
by siret ; if a;
run;
endrsubmit;
/* 1 275 570 obs */
rsubmit; 
proc sort data=nir_siret_ape out=xmiseret.nir_siret_ape; 
by nir;  run;
endrsubmit;
/* verif */
rsubmit;
proc sql;
select count(distinct(nir))
from xmiseret.nir_siret_ape; 
quit;
endrsubmit;
/* 1 275 570 obs */

/* -- ajout des codes NAF dans la table de base "cotisants_03_04_ape_siret" -- */
rsubmit;
data xmiseret.cotisants_03_04_ape_siret_plus;
merge xmiseret.cotisants_03_04_ape_siret (in=a)
	  xmiseret.nir_siret_ape;
by nir; if a;
run;
endrsubmit;
/* 1 275 570 obs */

/* d�nombrement sans filtre */
rsubmit;
proc sql;
select count(nir) as nb_nir
from xmiseret.cotisants_03_04_ape_siret_plus
/*where cd_naf_700 ne '' and siret = ''; 	/* ==> 0 obs */
/*where cd_naf_700 = '' and siret = ''; 	/* ==> 50 303 obs 		- 3.9% */
/*where cd_naf_700 ne '' and siret ne ''; 	/* ==> 1 183 440 obs 	- 92.7% */
/*where cd_naf_700 = '' and siret ne ''; 	/* ==> 41 827 obs 		- 3.3% */
/*where siret ne '' 	;					/* ==> 1 225 267 obs 	- 96% */
/*where cd_naf_700 ne ''  ;					/* ==> 1 183 440 obs 	- 92.7% */
quit;
endrsubmit;
/* 1 275 570 obs */

/*********************************************************************/
/* Enrichissement de la table de r�f�rence cotisants_03_04_ape_siret
   avec l'accord de branche correspondant au code APE 				 */
/*********************************************************************/

/* -- r�cup�ration de la table de correspondance entre accord de branche et code NAF
      table tri�e par cd_naf_700  -- */
data workx.corresp_naf_accord;
infile 'D:\Donn�es\mise retraite doffice\correspondance accord-code NAF.txt' delimiter='09'x MISSOVER DSD lrecl=32767 firstobs=2;
informat cd_naf_700 $4. ;
informat accord $200. ;
format cd_naf_700 $4. ;
format accord $200. ;
input cd_naf_700 accord $ ;
run;
rsubmit;
data xmiseret.corresp_naf_accord;
set corresp_naf_accord;
run;
endrsubmit;
/* --> 301 codes NAF avec accord correspondant 
   le groupe NAF presta services secteur tertiaire 
   inclut codes NAF de selection et fourniture de personnel ie
   74.5A=selection et mise � dispo de personnel et 74.5B=travail temporaire 
   --> 299 codes NAF avec accord correspondant 
   en supprimant les codes NAF 74.5A et 74.5B 
   du groupe NAF presta services secteur tertiaire  */
/* verif */
rsubmit;
proc sql;
select count(distinct(cd_naf_700))
from xmiseret.corresp_naf_accord; 
quit;
endrsubmit;
/* 301 codes NAF avec codes NAF 74.5A et 74.5B
   299 codes NAF sans codes NAF 74.5A et 74.5B */
rsubmit;
proc sql;
select count(distinct(accord))
from xmiseret.corresp_naf_accord; 
quit;
endrsubmit;
/* 37 accords diff�rents 
   --> OK avec fichier excel "correspondance d�rogations-codes NAF.xls"
       onglet "Liste tri�e par accord" */


/* -- affectation de l'accord � chaque code NAF et individu de la table de r�f�rence */
rsubmit; 
data corresp_nir_naf;
set xmiseret.cotisants_03_04_ape_siret_plus (keep=nir cd_naf_700 lib_naf_700); 
run;
endrsubmit;
/* 1 275 570 obs */
rsubmit; 
proc sort data=corresp_nir_naf ; 
by cd_naf_700;  run;
endrsubmit;
rsubmit;
data corresp_nir_naf_accord ;
merge corresp_nir_naf xmiseret.corresp_naf_accord;
by cd_naf_700 ; 
run;
endrsubmit;
/* denombrement */
rsubmit;
proc sql;
select count(nir) as nb_nir
from corresp_nir_naf_accord
/*where cd_naf_700 ne '' and accord = ''; 	/* ==> 560.672 obs */
/*where cd_naf_700 = '' and accord = ''; 	/* ==> 92.130 obs */
/*where cd_naf_700 ne '' and accord ne ''; 	/* ==> 622.768 obs */
where cd_naf_700 = '' and accord ne ''; 	/* ==> 0 obs */
quit;
endrsubmit;
/* verif: liste des codes naf non associ�s � une d�rogation */
rsubmit;
proc freq data=corresp_nir_naf_accord;
tables lib_naf_700;
where cd_naf_700 ne '' and accord = ''; 	
run;
endrsubmit;

/* -- appariement entre "cotisants_03_04_ape_siret_plus" et "corresp_naf_accord" */
rsubmit; 
proc sort data=corresp_nir_naf_accord out=xmiseret.corresp_nir_naf_accord; by nir;  run;
endrsubmit;
rsubmit;
data xmiseret.cotisants_03_04_ape_siret_accord;
merge xmiseret.cotisants_03_04_ape_siret_plus xmiseret.corresp_nir_naf_accord;
by nir ; 
run;
endrsubmit;
/* denombrement */
rsubmit;
proc sql;
select count(nir) as nb_nir
from xmiseret.cotisants_03_04_ape_siret_accord
where cd_naf_700 ne '' and accord = ''; 	/* ==> 560.672 obs - 44% */
/*where cd_naf_700 = '' and accord = ''; 	/* ==> 92.130 obs - 7.2% */
/*where cd_naf_700 ne '' and accord ne ''; 	/* ==> 622.768 obs - 48.8% */
/*where cd_naf_700 = '' and accord ne ''; 	/* ==> 0 obs */
quit;
endrsubmit;
/* 1 275 570 obs */
/* pr�s de 50% des assur�s cotisants en 2003/2004 sont concern�s par un accord de branche */



/*********************************************************************/
/* 								Statistiques 						 */
/*********************************************************************/

/* -- PARTIE I -- */
/* -- caract�risation des assur�s concern�s par un accord de branche -- */

rsubmit;
%macro titi(sexe=,cond1=,cond2=);
/* r�partition par sexe et accord */
%if (&sexe=1 or &sexe=2) %then %do;
proc freq data=xmiseret.cotisants_03_04_ape_siret_accord noprint;
tables accord/out=liste_naf_accord_&sexe.;
where sexe in ("&sexe.")
and accord ne ''
and &cond1 ; 
run;
%end;
%else %do;
proc freq data=xmiseret.cotisants_03_04_ape_siret_accord noprint;
tables accord/out=liste_naf_accord_&sexe.;
where accord ne ''
and &cond1 ; 
run;
%end;

/* s�lection des accords avec les effectifs par sexe les plus fr�quents (percent>1) */
proc sql;
		select accord into :liste_accord
		separated by ';'
		from liste_naf_accord_&sexe.
		where percent > 1;
		%let nb_obs=&sqlobs;
quit;
%put &nb_obs accords ;
%put &liste_accord;
%put ;
/* boucle sur le nombre d'accords les plus fr�quents */
%do i=1 %to &nb_obs;
/* d�termination du nom de l'accord */
%let argument&i=%scan(&liste_accord,&i,';');
%put &&argument&i;
/* effectifs par sexe et accords les plus repr�sent�s 
   + conditions sup sur g�n�rations et assur�s presta ou non */
	%if (&sexe=1 or &sexe=2) %then %do;
		proc freq data=xmiseret.cotisants_03_04_ape_siret_accord noprint;
		tables generation/out=liste_naf_accord_&sexe._accord&i.;
		where sexe in ("&sexe.")
		and accord = "&&argument&i"
		and (&cond1.)
		and (&cond2.); 
		run;
		proc sort data=liste_naf_accord_&sexe._accord&i.
		out=liste_naf_accord_&sexe._accord&i.b
		(rename=(count=freq_&sexe._accord&i. percent=part_&sexe._accord&i.)); 
		by generation; run;
	%end;
	%else %do;
		proc freq data=xmiseret.cotisants_03_04_ape_siret_accord noprint;
		tables generation/out=liste_naf_accord_&sexe._accord&i.;
		where accord eq "&&argument&i"
		and &cond1.
		and &cond2.; 
		proc sort data=liste_naf_accord_&sexe._accord&i.
		out=liste_naf_accord_&sexe._accord&i.b
		(rename=(count=freq_&sexe._accord&i. percent=part_&sexe._accord&i.)); 
		by generation; run;
	%end;

%end;

data liste_naf_accord_&sexe._gen_tot;
merge %do i=1 %to &nb_obs; liste_naf_accord_&sexe._accord&i.b %end; ;
by generation;
run;
proc transpose 	data=liste_naf_accord_&sexe._gen_tot 
				prefix=gen 
				out=liste_naf_accord_&sexe._gen_tot2(drop=_label_);
var %do i=1 %to &nb_obs; freq_&sexe._accord&i. part_&sexe._accord&i. %end; ;
id generation;
run;
data xmiseret.liste_naf_accord_&sexe._gen_tot;
set liste_naf_accord_&sexe._gen_tot2;
informat accord $200. ;
format accord $200. ;
select (_NAME_);
	%do i=1 %to &nb_obs; 
	when ("freq_&sexe._accord&i.","part_&sexe._accord&i.") accord="&&argument&i";
	%end;
	end;
drop _NAME_;
run;

%mend titi;
endrsubmit;

/* cond1 = filtre sur assur�s presta ou non 
   cond2 = filtre sur generation
   s�lection des assur�s ages de 60 � 66 ans entre 2006 et 2009
   soit les g�n�rations entre 1940 et 1949*/

/* --> assur�s prestataires + g�n�rations 1940 � 1960 (s'arr�te en fait � 1950) */
rsubmit;
%titi(sexe=1,cond1=%str(an_ej ne .),cond2=%str(1940<=generation<=1960));	/* 15 accords avec les effectifs les plus repr�sent�s (percent>1) */
%titi(sexe=2,cond1=%str(an_ej ne .),cond2=%str(1940<=generation<=1960));	/* 13 accords avec les effectifs les plus repr�sent�s (percent>1) */
%titi(sexe=3,cond1=%str(an_ej ne .),cond2=%str(1940<=generation<=1960));	/* 14 accords avec les effectifs les plus repr�sent�s (percent>1) */
endrsubmit;
/* --> assur�s non prestataires + g�n�rations 1940 � 1960 (pour garder les actifs seniors)  */
rsubmit;
%titi(sexe=1,cond1=%str(an_ej=.),cond2=%str(1940<=generation<=1960));	/* 15 accords avec les effectifs les plus repr�sent�s (percent>1) */
%titi(sexe=2,cond1=%str(an_ej=.),cond2=%str(1940<=generation<=1960));	/* 13 accords avec les effectifs les plus repr�sent�s (percent>1) */
%titi(sexe=3,cond1=%str(an_ej=.),cond2=%str(1940<=generation<=1960));	/* 14 accords avec les effectifs les plus repr�sent�s (percent>1) */
endrsubmit;
/* --> assur�s presta/non presta + g�n�rations 1940 � 1960 (pour garder les actifs seniors)  */
rsubmit;
%titi(sexe=1,cond1=%str(an_ej=. or an_ej ne .),cond2=%str(1940<=generation<=1960));	/* 15 accords avec les effectifs les plus repr�sent�s (percent>1) */
%titi(sexe=2,cond1=%str(),cond2=%str(1940<=generation<=1960));	/* 13 accords avec les effectifs les plus repr�sent�s (percent>1) */
%titi(sexe=3,cond1=%str(),cond2=%str(1940<=generation<=1960));	/* 14 accords avec les effectifs les plus repr�sent�s (percent>1) */
endrsubmit;

/* export pour sorties fichiers excel */
/* --> r�partition par sexe et accord */
proc export 
data = workx.liste_naf_accord_1
outfile = 'D:\Donn�es\mise retraite doffice\eff_naf_accord_1.txt'  replace;
run;
proc export 
data = workx.liste_naf_accord_2
outfile = 'D:\Donn�es\mise retraite doffice\eff_naf_accord_2.txt'  replace;
run;
proc export 
data = workx.liste_naf_accord_3
outfile = 'D:\Donn�es\mise retraite doffice\eff_naf_accord_3.txt'  replace;
run;
/* --> r�partition par sexe et accord et g�n�ration et presta/non presta */
proc export 
data = miseret.liste_naf_accord_1_gen_tot
outfile = 'D:\Donn�es\mise retraite doffice\eff_accord_1_gen_tot.txt'  replace;
run;
/* 30 obs */
proc export 
data = miseret.liste_naf_accord_2_gen_tot
outfile = 'D:\Donn�es\mise retraite doffice\eff_accord_2_gen_tot.txt'  replace;
run;
/* 26 obs */
proc export 
data = miseret.liste_naf_accord_3_gen_tot
outfile = 'D:\Donn�es\mise retraite doffice\eff_accord_3_gen_tot.txt'  replace;
run;
/* 28 obs */



/* -- PARTIE II -- */




/* -- PARTIE III -- */
/* -- d�nombrement des prestataires -- */
rsubmit;
proc freq data = xmiseret.cotisants_03_04_ape_siret_accord;
tables an_ej/missing;
run;
endrsubmit;
/* 1 236 949 cotisants (97%) 
      38 621 prestataires (3%) entre 1967 et 2006 avec maximum des effectifs en
		2004 = 11 938 presta (1%)
		2005 = 12 128 presta (1%) 
        soit environ 10 000 presta avant 2004 */

/* -- simulation sur les prestataires du flux 2004 -- */




/*
   ---> conservation des assur�s non prestataires ou prestataires � partir de 2005  
   ---> Conservation des assur�s avec code et libell� NAF 
   ---> comme les d�rogations de mise � la retraite d'office concernent 
		- des assur�s < 65 ans et potentiellement en RA >= 56 ans
		- jusqu'� fin 2009
	   il convient de restreindre l'analyse aux cotisants en 2004 
	   �g�s entre 51 et 66 ans en 2004 (�ge r�volu en fin d'ann�e)			*/
