/** Connexion **/
%let srv=SasSrvConnect.n18.an.cnav 7551;
option comamid=tcp remote=srv;
signon noscript user=_prompt_ password=_prompt_;

rsubmit;
libname xtrav '/travail8/z078886';
endrsubmit;
libname trav slibref=xtrav server=srv;
libname xwork slibref=work server=srv;

* load data;
libname chemin 'T:\data';
data xwork.panelts;
set chemin.panelts_eir12;
run;

rsubmit;
data panelts;
set panelts;
ind_cc = 0;
ind_apen=0;
if apen ne "" or     then ind_apen=1;
if conv_coll ne . then ind_cc=1;
run;
proc freq data=panelts (where=(ind_apen=1));
table ind_cc*an;
run;
endrsubmit;


* filters: keep only after 2005 and relevant variables;
rsubmit;
data panelts_mro 
	(keep= noind dept regt an annai sx debremu finremu netnet apen apen2 apet apet2 nbsa_et nbsa_ent entsir conv_coll);
set panelts;
if an>=2005;
run;
endrsubmit;


/**** Save output ****/
libname O 'F:\EIR 2012\';
data O.panelts_mro;
 set xwork.panelts_mro; 
run;
