/***************************************************************************************************************************************************************************
*
*  MRO: version 2. PART II (FULL RATE)
*  We keep only work history, merge with NAF is done on R. 
*  Two datesets are saved: one with (fixed) individual information and the other with all the work history between 

/** Connexion **/
%let srv=SasSrvConnect.n18.an.cnav 7551;
option comamid=tcp remote=srv;
signon noscript user=_prompt_ password=_prompt_;

rsubmit;
libname xtrav '/travail8/z078886';
endrsubmit;
libname trav slibref=xtrav server=srv;
libname xwork slibref=work server=srv;

%INCLUDE "O:\bunching\Echant20e_2014\formats.sas";


%macro exportxls (fichier,chemin,onglet,typop);
proc export data=xwork.&fichier.
   outfile=&chemin.
   dbms=excel &typop.;
   sheet=&onglet.;
run;
%mend;



/***** I. Work on data ********/

/*** I.1 Carrer
Sample selection: we keep only individuals:
- retiring between 2000 and 2012  
- with last employer=RG
***/


** I.1.a filters;
/* Filters 1:  Variables + generations (until 1950)*/
rsubmit;
%macro filters1();
data work.temp;
set xtrav.Table_init_ech14_20e_avtc_eval (keep=    ident sexe nb_enf generation trim_nais mois_nais an_deces prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali categ
       %do i=1947 %to 2014;  rep_&i._1--tr_&i._4  plaf_&i.--avpf_&i.  %end ; );
if generation>=1932;
if generation<=1955;
run;
%mend;      
endrsubmit;
rsubmit;
%filters1();
endrsubmit;

/* Filters 2: keep those with RG as the last employer */
rsubmit;
data work.temp2;
set  work.temp; 
/* Arrays */
array sal(1947:2014,4) plaf_1947--avpf_2014 ;
array rep(1947:2014,2,4) rep_1947_1--tr_2014_4 ;
* Variables;
an_finact   =0;  * year of last report;
age_finact  =0;  * age of last report;
last_rep    =''; * type of last report;
an_60       =0;  * year of 60;
do a = 1970 to 2014; * Loop on years;
age=a-generation;
* Type of last report before liq;
if (a<an_ej or an_ej=.) and (rep(a,1,1)='e' or rep(a,1,1)='h' or rep(a,1,1)='k')  then  last_rep = rep(a,1,1);
* Year of 60 and last report if retired;
if age=60                      then an_60=a;
if sal(a,1)>0 and (a<=an_ej or an_ej=.)     then do;
an_finact=a; 
age_finact=age;  
end;
end;
run;
endrsubmit;


* Filters: those with last employment in RG employed at 55 ;
rsubmit;
data work.temp;
set  work.temp2; 
if last_rep ='e';
if age_finact>=55;
run;
endrsubmit;



** I.1.b variables creation **;
rsubmit;
data work.temp;
set  work.temp; 
* Retirement age; 
ageliq_month = ( an_ej+(mois_ej-1)/12 ) -  (generation + (mois_nais-1)/12 ) + 1/12;
if prem_jour^='1' then ageliq_month=ageliq_month-1/12;
t = ageliq_month-floor(ageliq_month);
t = round(t*12)+1  ;
t = floor((t-1)/3) ;
ageliq_trim2 = floor(ageliq_month) + t*0.25 ;
ageliq_ann   = floor(ageliq_trim2);
* required duration;
duree_req=0;
if generation<=1933 & an_ej <2003 then duree_req=150 ;
if generation=1934 & an_ej <2003 then duree_req=151 ;
if generation=1935 & an_ej <2003 then duree_req=152 ;
if generation=1936 & an_ej <2003 then duree_req=153 ;
if generation=1937 & an_ej <2003 then duree_req=154 ;
if generation=1938 & an_ej <2003 then duree_req=155 ;
if generation=1939 & an_ej <2003 then duree_req=156 ;
if generation=1940 & an_ej <2003 then duree_req=157 ;
if generation=1941 & an_ej <2003 then duree_req=158 ;
if generation=1942 & an_ej <2003 then duree_req=159 ;
if an_ej >=2003                  then duree_req=160 ;
if generation=1943               then duree_req=160 ;
if generation=1944               then duree_req=160 ;
if generation=1945               then duree_req=160 ;
if generation=1946               then duree_req=160 ;
if generation=1947               then duree_req=160 ;
if generation=1948               then duree_req=160 ;
if generation=1949               then duree_req=161 ;
if generation=1950               then duree_req=162 ;
if generation=1951               then duree_req=163 ;
if generation=1952               then duree_req=164 ;
if generation=1953               then duree_req=165 ;
if generation=1954               then duree_req=165 ;
if generation=1955               then duree_req=166 ;
* distDA;
dist_DA = valid_tot-duree_req;
* distFR;
trim_dec1 = max(0,-dist_DA);
trim_dec2 = max(0,65-ageliq_trim2)*4;
trim_dec  = min(trim_dec1,trim_dec2)  ;
trim_sur1 = max(0,dist_DA);
trim_sur2 = max(0,ageliq_trim2-60)*4;
trim_sur  = min(trim_sur1 ,trim_sur2);
dist_FR   = -trim_dec + trim_sur ;
* an_FR;
an_FR_1   = floor(an_ej+(trim_ej-1)/4 - dist_FR/4);
age_FR_1  = an_FR_1   - generation;
run;
endrsubmit;

rsubmit;
proc freq data=temp;
table trim_dec1 trim_dec2;
run;
endrsubmit;

rsubmit;
data test;
set temp;
if valid_tot<duree_req and an_ej^=. and ageliq_trim2<59;
run;
endrsubmit;



*** Computation of ID60 and an_FR using the retrospective calendar  ***;
rsubmit;
data work.temp;
set  work.temp; 
/* Arrays */
array rep(1947:2014,2,4) rep_1947_1--tr_2014_4 ;
array sal(1947:2014,4)   plaf_1947--avpf_2014  ;
/* Initialisation */
ID60        = valid_tot; 
count_valid = valid_tot; 
an_dureereq = 5555; *year at which the required duration is reached;
age_dureereq= 65;
* Loop on years;
do a = 2014 to 1980 by -1; 
   age_ann = a - generation;
   incr1 =0;
   incr2 =0;
   mil=0;mal=0;cho=0;cot=0;ali=0;nali=0;avp=0;equ=0;aut=0;inv=0;cot2=0; 
   do t=1 to 4;       * Loop on nb of reports ;
   select (rep(a,1,t));
                        when ('a')    mil=sum(mil,input(rep(a,2,t),2.)); 
                        when ('b')    mal=sum(mal,input(rep(a,2,t),2.));
                        when ('c')    cho=sum(cho,input(rep(a,2,t),2.));
                        when ('h')    ali=sum(ali,input(rep(a,2,t),2.));
                        when ('k')    nali=sum(nali,input(rep(a,2,t),2.));
                        when ('g')    equ=sum(equ,input(rep(a,2,t),2.));
                        when ('d')    aut=sum(aut,input(rep(a,2,t),2.));
                        when ('e')    cot2=sum(cot2,input(rep(a,2,t),2.));
                        otherwise;
                        end;
     end; * end loop on nb of reports;
     if sal(a,1)>0 then cot=int(sal(a,1)/input(put(a*10000 + sum( an_ej, 0.25*int(sum(mois_ej,-1)/3 +1) ) * ( 0<an_ej<5555 and a<an_ej ) ,h150sal_min.),best12.));
     if sal(a,4)>0 then avp=int(sum(sal(a,1),sal(a,4))/input(put(a*10000 + sum( an_ej, 0.25*int(sum(mois_ej,-1)/3 +1) ) * ( 0<an_ej<5555 and a<an_ej ) ,h150sal_min.),best12.));
	incr1= min(sum(0,mil,mal,cho,cot*(avp<=0),avp,ali,nali,equ,aut,inv),4);
	incr2= min(sum(0,mil,mal,cho,cot2        ,avp,ali,nali,equ,aut,inv),4);
    /* Increment for an_FR*/
	* Test if duree_req reached this year;
	if count_valid>= duree_req then do;
	an_dureereq= a;
	age_dureereq=age_ann;
	end;
	* Increment (backward) of the duration);
	if a = an_ej then  count_valid      =sum( count_valid , -min(incr1,trim_ej-1));
	if a < an_ej then  count_valid      =sum( count_valid ,-incr1);
	/* Increment dependent on if age=60 or a=an_ej*/
	if      a = an_ej   then ID60   =sum( ID60       , -min(incr1,trim_ej-1));
	else if age_ann> 60 then ID60   =sum ( ID60      , -incr1);
	else if age_ann= 60 then ID60   =sum( ID60       , -min(incr1,trim_nais-1));
 end;  * end loop on year;
* Age and an TR ;
age_FR=max(60,	age_dureereq);
age_FR=min(65,	age_FR);
an_FR = generation + age_FR;
run;
endrsubmit;

rsubmit;
proc freq data=temp;
table age_FR /missing;
run;
endrsubmit;


rsubmit;
data work.temp;
set  work.temp; 
/* Arrays */
** Groups;
group=0;
if (ID60>=duree_req)						 then  group=1;  	*FR reached at 60;
if (duree_req-20<=ID60 & ID60<duree_req)     then  group=2;   *FR reached bw 60 and 65;	
if (ID60 <duree_req-20)                      then  group=3;   *FR reached after 65;
group_1=0;
ID601        = valid_tot - max(0,(ageliq_trim2-60)*4);
if (ID601>=duree_req)						 then  group_1=1;  	*FR reached at 60;
if (duree_req-20<=ID601 & ID601<duree_req)   then  group_1=2;   *FR reached bw 60 and 65;	
if (ID601 <duree_req-20)                     then  group_1=3;   *FR reached after 65;
group_2=0;
run;
endrsubmit;


rsubmit;
data workers;
set  temp; 
endrsubmit;

/*** I.2 Merge with employers information ***/
/** I.2.a Merge datasets */ 
rsubmit;
%macro filters3();
data work.emp;
set xtrav.ech2014_enreg6_hades (keep=    ident  %do i=2000 %to 2014;  nu_emp_&i._1 nu_emp_&i._2 act_etab_&i._1 act_etab_&i._2 cd_emp_&i._1  cd_emp_&i._2 raison_sociale_&i._1  raison_sociale_&i._2 cd_ape_etab_&i._1  dt_fin_&i._1 cd_ape_etab_&i._2 dt_fin_&i._2 na_dcl_sal_&i._1 na_dcl_sal_&i._2 %end ; );
* Recode when 1 is MV and 2 is not;
%do i=2000 %to 2014;
%let j=%eval(&i.-1);
if (cd_ape_etab_&i._1='' and cd_ape_etab_&i._2 ne '') then do;
cd_ape_etab_&i._1=cd_ape_etab_&i._2;
cd_emp_&i._1=cd_emp_&i._2;  
dt_fin_&i._1=dt_fin_&i._2;
raison_sociale_&i._1= raison_sociale_&i._2;
na_dcl_sal_&i._1= na_dcl_sal_&i._2;
end;
naf_&i=cd_ape_etab_&i._1;
%end;
run;
%mend;      
endrsubmit;
rsubmit;
%filters3();
endrsubmit;


** Correction !! employ�s de maison enregistr�s � l'URSAFF avant 2002! *;
rsubmit;
%macro correctNAF();
data emp;
set  emp;
%do i=2000 %to 2002;
if (raison_sociale_&i._1="URSSAF" and na_dcl_sal_&i._1="06") then do;
raison_sociale_&i._1="";
naf_&i.="";
end;
%end;
run;
%mend;      
endrsubmit;
rsubmit;
%correctNAF();
endrsubmit;


rsubmit;
data base;
merge workers  (in=main)
	  emp      (in=b);
by ident; 
if main;
run;
endrsubmit;






rsubmit;
data xtrav.temp;
set base ;
run;
endrsubmit;

/**** II. Save output ****/

/**** II.1 ALL ****/
/** One line by individual **/

libname O 'O:\bunching\MRO\data\';
libname N 'N:\DSPR-Simon\data\';
data O.data_all_2;
 set trav.temp (keep=    ident sexe nb_enf generation trim_nais mois_nais  prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali ID60 ID601 dist_DA 
                          ageliq_month ageliq_trim2 an_finact age_finact an_60 an_FR an_FR_1 group group_1 dist_FR); 
run;

/** Wide: one line by year **/
rsubmit;
%macro filters4();
data temp;
set  xtrav.temp (keep=   ident sexe  generation an_ej 
                          ageliq_month an_finact age_finact
       						%do i=2000 %to 2014;   naf_&i. plaf_&i. %end ; );
run;
%mend;      
endrsubmit;
rsubmit;
%filters4();
endrsubmit;

rsubmit;
proc freq data=temp;
table age_finact*an_finact;
run;
endrsubmit;

* long to wide;
rsubmit;
data wide1;
set  temp (keep= ident naf_2000 naf_2001 naf_2002 naf_2003 naf_2004 naf_2005 naf_2006 
				 	   naf_2007 naf_2008 naf_2009 naf_2010 naf_2011 naf_2012 naf_2013 );
run;
proc sort data=work.wide1 ; by ident  ; run;
data wide2;
set  temp (keep= ident plaf_2000 plaf_2001 plaf_2002 plaf_2003 plaf_2004 plaf_2005 plaf_2006 
				 	   plaf_2007 plaf_2008 plaf_2009 plaf_2010 plaf_2011 plaf_2012 plaf_2013 );
run;
proc sort data=work.wide2 ; by ident  ; run;
data indiv;
set  temp (keep=     ident sexe  generation an_ej 
                          ageliq_month an_finact age_finact);
run;
proc transpose data=work.wide1  out=work.long1;
var naf_2000 naf_2001 naf_2002 naf_2003 naf_2004 naf_2005 naf_2006 
	naf_2007 naf_2008 naf_2009 naf_2010 naf_2011 naf_2012 naf_2013 ;
by ident ;
run;
data long1;
 set long1 (rename=(col1=naf));
 year=input(substr(_name_, 5), 5.);
 drop _name_;
run; 
proc transpose data=work.wide2  out=work.long2;
var plaf_2000 plaf_2001 plaf_2002 plaf_2003 plaf_2004 plaf_2005 plaf_2006 
	plaf_2007 plaf_2008 plaf_2009 plaf_2010 plaf_2011 plaf_2012 plaf_2013  ;
by ident ;
run;
data long2;
 set long2 (rename=(col1=plaf));
 year=input(substr(_name_, 6), 5.);
 drop _name_;
run; 

proc sort data=work.long1 ; by ident  ; run;
proc sort data=work.long2 ; by ident  ; run;
proc sort data=work.indiv ; by ident  ; run;
data long;
merge long1 long2;
by ident; 
run;

endrsubmit;

rsubmit; 
data base;
merge long  (in=main)
	  indiv (in=b);
by ident; 
if main;
run;
endrsubmit;

/* Variables: dummies for, retire a given year, claim pension a given year */
rsubmit; 
data base;
set base;
t_liq   = 0;
  liq   = 0;
t_finact= 0;
  finact= 0;
if an_ej=year      then t_liq=1;
if an_ej<=year     then   liq=1;
if an_finact=year  then t_finact=1;
if an_finact<=year then   finact=1;
run;
endrsubmit;




/* check : sal>0 et no naf*/
rsubmit;
data check ;
set base;
if plaf>0;
run;
proc sort data=check; by naf ; run;
proc freq data=check;
table code_naf /missing;
run;
endrsubmit;



/* drop observations with an_finact after 2000 and age <68 age >50 and year until fin act only*/
rsubmit; 
data base;
set base;
if an_finact>=2000;
if year-generation>=59;
if generation>=1934;
*if year<=an_finact+1;
run;
endrsubmit;


libname N 'N:\DSPR-Simon\data\';
*libname O 'O:\bunching\MRO\data\';
data N.data_long_all_2;
set  xwork.base; 
run;



/**** II.2 Filters for FR analysis ****/

*Only filters: categ;
rsubmit; 
data work.temp;
set  xtrav.temp; 
if an_ej^=.;
if categ='';
if an_finact>=2000;
if generation>=1939;
run;
endrsubmit;

/** One line by individual **/
libname O 'O:\bunching\MRO\data\';
libname N 'N:\DSPR-Simon\data\';
data N.data_v2;
 set xwork.temp (keep=    ident sexe nb_enf generation trim_nais mois_nais  prem_jour an_ej trim_ej mois_ej valid_tot valid_rg valid_ali ID60 ID601 dist_DA 
                          ageliq_month ageliq_trim2 an_finact age_finact an_60 an_FR an_FR_1 group group_1 dist_FR); 
run;

/** Wide: one line by year **/
* variable selection;
rsubmit;
%macro filters4();
data temp;
set  work.temp (keep=    ident generation 
       						%do i=2000 %to 2014;   naf_&i. %end ; );
run;
%mend;      
endrsubmit;
rsubmit;
%filters4();
endrsubmit;
* long to wide;
rsubmit;
data wide1;
set  temp (keep= ident naf_2000 naf_2001 naf_2002 naf_2003 naf_2004 naf_2005 naf_2006 
				 	   naf_2007 naf_2008 naf_2009 naf_2010 naf_2011 naf_2012 naf_2013 naf_2014 );
run;
proc sort data=work.wide1 ; by ident  ; 
run;
data indiv;
set  temp (keep=    ident sexe generation );
run;
proc transpose data=work.wide1  out=work.long1;
var naf_2000 naf_2001 naf_2002 naf_2003 naf_2004 naf_2005 naf_2006 
	naf_2007 naf_2008 naf_2009 naf_2010 naf_2011 naf_2012 naf_2013 naf_2014 ;
by ident ;
run;
data long1;
 set long1 (rename=(col1=naf));
 year=input(substr(_name_, 5), 5.);
 drop _name_;
run; 
endrsubmit;
rsubmit;
proc sort data=work.long1 ; by ident  ; run;
proc sort data=work.indiv ; by ident  ; run;
data base;
merge long1  (in=main)
	  indiv (in=b);
by ident; 
if main;
run;
endrsubmit;

/* Variables: dummies for, retire a given year, claim pension a given year */
rsubmit; 
data base;
set base;
t_liq   = 0;
  liq   = 0;
t_finact= 0;
  finact= 0;
if an_ej=year      then t_liq=1;
if an_ej<=year     then   liq=1;
if an_finact=year  then t_finact=1;
if an_finact<=year then   finact=1;
run;
endrsubmit;

/* drop observations after retirement  */
rsubmit; 
data base;
set base;
if year<=an_ej;
run;
endrsubmit;


/* check : sal>0 et no naf*/
rsubmit;
data check (where=(an_finact=year));
set base;
run;
proc freq data=check;
table code_naf /missing;
by year;
run;
endrsubmit;


libname N 'N:\DSPR-Simon\data\';
data N.data_long_v2;
set  xwork.base; 
run;
