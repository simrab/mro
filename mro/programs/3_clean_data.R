
########################################### Data generation ############################################

## Program: Load data from SAS "raw" data (see 1_export_data.sas for the program), data cleaning and sample selection. 
# Outline: 
# 0. Initialisation: load program and functions 
# I. 


### 0. Packages and functions ###
source (paste0(wd,"0_Utils.R"))


merge_and_clean_data = function(data_ind, data_long, table_naf_mro, naf1_naf2, table_pct)
{
  ## Merge indiv and long data ####
  # A deplacer dans SAS
  data_ind$age_ej_mois <- data_ind$an_ej- data_ind$generation+ (data_ind$mois_ej- data_ind$mois_nais-1)/12
  # Indiv variables
  list_var         <- union("ident",setdiff(names(data_ind),names(data_long)))
  data_long  <- merge(data_long    ,data_ind[,list_var] , by="ident",all.x=T)
  # Variable corrections
  data_long$age        <-  data_long$year- data_long$generation
  data_long$age_finact <-  data_long$an_finact- data_long$generation
  data_long$sexe       <-  as.numeric(data_long$sexe) -1
  data_long$ind_mro    <- NULL  
  
  ## Recode NAF1 into NAF2 ####
  data_long         <- data_long[order(data_long$ident,data_long$year),]
  data_long$naf     <- as.character(data_long$naf)
  data_long$naf_old <- data_long$naf 
  naf1    <- data_long[which(is.element(data_long$naf,naf1_naf2$code_naf1)),
                       c("naf","ident","year")]
  naf1to2 <- merge(naf1,naf1_naf2[,c("code_naf2","code_naf1")],   
                   by.x='naf',by.y="code_naf1",all.x=T)
  naf1to2         <- naf1to2[order(naf1to2$ident,naf1to2$year),]
  data_long$naf[which(is.element(data_long$naf,naf1_naf2$code_naf1))] <- naf1to2$code_naf2
  # Code industry
  l1 <- unique(data_long$naf)
  l2 <- unique(naf1_naf2$code_naf2)
  n  <- naf1_naf2[which(is.element(naf1_naf2$code_naf2,l1)),c("code_naf2", "code_industry_n1","code_industry_n2",
                                                              "code_industry_n3","code_industry_n4")]
  # Unique element of naf1_naf2 
  n  <- n[which(!duplicated(n$code_naf2)),]
  add <- as.data.frame(cbind(setdiff(l1,l2),matrix(NA,ncol=4,nrow=length(setdiff(l1,l2)))))
  names(add) <- c("code_naf2", "code_industry_n1","code_industry_n2","code_industry_n3","code_industry_n4")
  n   <- rbind(n,add)
  data_long <- merge(data_long,n,
                     by.x="naf",by.y="code_naf2",all.x=T)
  
  
  ## Merge with DA with cc variables ####
  table_naf_mro = table_naf_mro[!duplicated(table_naf_mro),]
  data_long  <- merge(data_long, table_naf_mro,
                      by.x="naf",by.y="naf2",all.x=T)
  data_long  <- data_long[order(data_long$ident,data_long$year),]
  
  # NB: NAF in DATA_INIT THAT ARE NOT IN TABLE_NAF_MRO => to check
  l1 <- unique(data_long$naf)
  l2 <- unique(c(naf1_naf2$code_naf1,naf1_naf2$code_naf2))
  setdiff(l1,l2)
  table(data_long$year[which(is.element(data_long$naf,setdiff(l1,l2)) & data_long$naf!="")])
  
  ## Merge with table_pct 
  data_long = merge(data_long, table_pct, by.x = c("naf", "year"),  by.y = c("naf2", "year"), all.x = T)
  data_long  <- data_long[order(data_long$ident,data_long$year),]
  
  ## Variable creations ####
  ## Change naf ##
  data_long$naf_char <- as.character(data_long$naf)
  data_long$naf_num  <- 0
  data_long$naf_num[nchar(data_long$naf_char)==4]  <- substr(data_long$naf_char[nchar(data_long$naf_char)==4] ,1,3)
  data_long$naf_num[nchar(data_long$naf_char)==5]  <- substr(data_long$naf_char[nchar(data_long$naf_char)==5] ,1,4)
  data_long$naf_num  <- as.numeric(data_long$naf_num )
  data_long$naf_next <- ave(data_long$naf_num, data_long$ident, FUN = shift1)
  data_long$exit_naf <- ifelse(data_long$naf_num==data_long$naf_next,0,1)
  data_long$exit_naf[data_long$last==1]<-1
  data_long$naf_char = NULL
  
  ## T variable
  data_long$T1 = data_long$T1_1
  
  ## DA cible  
  data_long$DA_cible = 150
  data_long$DA_cible[data_long$generation >= 1934] = 151
  data_long$DA_cible[data_long$generation >= 1935] = 152
  data_long$DA_cible[data_long$generation >= 1936] = 153
  data_long$DA_cible[data_long$generation >= 1937] = 154
  data_long$DA_cible[data_long$generation >= 1938] = 155
  data_long$DA_cible[data_long$generation >= 1939] = 156
  data_long$DA_cible[data_long$generation >= 1940] = 157
  data_long$DA_cible[data_long$generation >= 1941] = 158
  data_long$DA_cible[data_long$generation >= 1942] = 159
  data_long$DA_cible[data_long$generation >= 1943] = 160
  data_long$DA_cible[data_long$generation >= 1949] = 161
  data_long$DA_cible[data_long$generation >= 1950] = 162
  data_long$DA_cible[data_long$generation >= 1951] = 163
  data_long$DA_cible[data_long$generation >= 1952] = 164
  data_long$DA_cible[data_long$generation >= 1953] = 165
  data_long$DA_cible[data_long$generation >= 1955] = 166
  

  
  ## Age
  data_long$decal = 0
  data_long$decal[data_long$generation == 1951 & data_long$mois_nais>6] = 4/12
  data_long$decal[data_long$generation == 1952] = 9/12
  data_long$decal[data_long$generation == 1953] = 1+2/12
  data_long$decal[data_long$generation == 1954] = 1+7/12
  data_long$decal[data_long$generation >= 1955] = 2
  data_long$aad = 65 + data_long$decal 
  data_long$aod = 60 + data_long$decal 
  
  ## Next 
  data_long$naf_next    <- ave(data_long$naf_num,data_long$ident,FUN=shift1)
  data_long$exit_naf    <- ifelse(data_long$naf_num==data_long$naf_next,0,1)
  data_long$exit_emp    <- ifelse(data_long$naf_next==0,1,0)
  data_long$naf_to_naf  <- ifelse(data_long$naf_next!=0 & data_long$exit_naf==1, 1, 0)
  data_long$sal_next   <- ave(data_long$deplaf, data_long$ident, FUN=shift1)
  data_long$exit_sal   <- ifelse(data_long$sal_next==0,1,0)
  ## Bef 
  data_long$naf_bef    <- ave(data_long$naf_num,data_long$ident,FUN=shiftm1)
  data_long$entry_naf  <- ifelse(data_long$naf_num==data_long$naf_bef,0,1)
  data_long$entry_naf_from_naf  <- ifelse(data_long$entry_naf==1 & data_long$naf_bef != "",1,0)
  data_long$entry_naf_from_noth  <- ifelse(data_long$entry_naf==1 & data_long$naf_bef == "",1,0)
  #data_long$naf_bef <- NULL
  
  # Pension claiming 
  data_long$claming   <- ifelse(data_long$year==data_long$an_ej | data_long$year==data_long$an_ej-1 ,1,0)
  

  return(data_long)
}  



variables_creation = function(main_data)
{
  main_data$T1 = main_data$T_c1_p1_s1
  main_data$year_ccT1 = main_data$year_c1_p1_s1
  main_data$DA         <- ifelse(main_data$T1==1,1,0)
  main_data$aft2005    <- ifelse(main_data$year>=2006,1,0)
  main_data$aftDA      <- ifelse(main_data$year>=main_data$year_ccT1,1,0)
  main_data$y_DA       <- ifelse(main_data$year==main_data$year_ccT1,1,0)
  main_data$y_DA_p1    <- ifelse(main_data$year==main_data$year_ccT1+1,1,0)
  main_data$y_DA_p2    <- ifelse(main_data$year==main_data$year_ccT1+2,1,0)
  main_data$y_DA_p3    <- ifelse(main_data$year>=main_data$year_ccT1+3,1,0)
  main_data$y_DA_m1    <- ifelse(main_data$year==main_data$year_ccT1-1,1,0)
  main_data$y_DA_m2    <- ifelse(main_data$year==main_data$year_ccT1-2,1,0)
  main_data$y_DA_m3    <- ifelse(main_data$year==main_data$year_ccT1-3,1,0)
  main_data$aftDA[is.na(main_data$aftDA)]           <- 0
  main_data$y_DA[is.na(main_data$y_DA)]       <- 0
  main_data$y_DA_p1[is.na(main_data$y_DA_p1)] <- 0
  main_data$y_DA_p2[is.na(main_data$y_DA_p2)] <- 0
  main_data$y_DA_p3[is.na(main_data$y_DA_p3)] <- 0
  main_data$y_DA_m1[is.na(main_data$y_DA_m1)] <- 0
  main_data$y_DA_m2[is.na(main_data$y_DA_m2)] <- 0
  main_data$y_DA_m3[is.na(main_data$y_DA_m3)] <- 0

  
  # First and last obs
  main_data$a     <- 1
  main_data$b     <- ave(main_data$a,main_data$ident,FUN=cumsum)
  main_data$c     <- ave(main_data$a,main_data$ident,FUN=sum)
  main_data$first <- ifelse(main_data$b==1,1,0)
  main_data$last  <- ifelse(main_data$b==main_data$c,1,0)
  
  # Full rate variables
  main_data$an_TR <- main_data$an_FR2
  main_data$FR    <- ifelse(main_data$year >= main_data$an_TR,1,0)
  
  # Control variables
  main_data$D_age      <- as.character(main_data$age)
  main_data$mean_wage  <- main_data$mean_sal_last15/10000
  main_data$eff        <- as.numeric(as.character(main_data$effectif))
  main_data$D_year     <- factor(main_data$year)
  main_data$D_dep      <- factor(main_data$dep)
  main_data$D_cc       <- as.character(main_data$cc)
  main_data$D_naf      <- factor(main_data$naf)
  main_data$D_ind1     <- factor(main_data$code_industry_n1)
  main_data$D_ind2     <- factor(main_data$code_industry_n2)
  main_data$D_ind3     <- factor(as.numeric(main_data$code_industry_n3)*10)
  main_data$D_ind4     <- factor(as.numeric(main_data$code_industry_n4)*100)
  main_data$D_id       <- factor(main_data$ident)
  main_data$pension    <- main_data$mt_dp/10000
  
  main_data$small_firm = ifelse(main_data$size < 50, 1, 0)
  main_data$pension_incentive = main_data$valid_rg*main_data$taux/10000
  
  return(main_data)
}  



####### II. Cleaning data #####


## II. 1 Main sample ####

# Load data
load(paste0(path_data,    "raw/data_ind_ech16_v8.Rdata" )) 
load(paste0(path_data,    "raw/data_long_ech16_v8.Rdata")) 
load(paste0(path_accord,  "naf_DA_v11.Rdata"   ))
load(paste0(path_accord,  "naf1_naf2_v11.Rdata"))
load(paste0(path_accord,  "naf_pct_mro_v11.Rdata"))

# Cleaning data
main_data = merge_and_clean_data(data_ind, data_long, table_naf_mro, naf1_naf2, table_pct)

# Sample selection 
c(length(data_long$ident),length(unique(main_data$ident)))
filters      <- unique(main_data$ident[which(main_data$age_finact>=59)])
main_data <- main_data[which(is.element(main_data$ident,filters)),]
# c(length(main_data$ident),length(unique(main_data$ident)))
# main_data <- main_data[which(main_data$year<=main_data$an_finact),]
c(length(main_data$ident),length(unique(main_data$ident)))
main_data <- main_data[which(main_data$age>=59 & main_data$age<70 ),]
c(length(main_data$ident),length(unique(main_data$ident)))

# Variable creation
main_data <- variables_creation(main_data)

# Save data
save(main_data, file=paste0(path_data,"clean/main_data_e16_a11_v8.Rdata"))    


## II. 2 Hiring sample ####

# Load data
load(paste0(path_data,    "raw/data_ind_ech15_hiring_v1.Rdata"))
load(paste0(path_data,    "raw/data_long_ech15_hiring_v1.Rdata"))
load(paste0(path_accord,  "naf_DA_v10.Rdata"  ))
load(paste0(path_accord,  "naf1_naf2_v10.Rdata"  ))

# Cleaning data
# list_deleted = c( "prem_jour", "pays_nais", "prem_rep")
# for (v in list_deleted){data_ind_hiring[, v] = NULL}


data_long_hiring$dep  = NULL
data_long_hiring$age = data_long_hiring$year - data_long_hiring$generation
data_long_hiring = data_long_hiring[which(data_long_hiring$age >= 49 & data_long_hiring$age <= 60),]
data_ind_hiring = data_ind_hiring[which(is.element(data_ind_hiring$ident, data_long_hiring$ident)),]

data_long_hiring$deplaf = data_long_hiring$plaf

hiring_data = merge_and_clean_data(data_ind_hiring, data_long_hiring, table_naf_mro, naf1_naf2)

# Variable creation
hiring_data$an_FR2 = 9999
hiring_data$mt_dp = 0
hiring_data <- variables_creation(hiring_data)

# Sample selection
c(length(hiring_data$ident),length(unique(hiring_data$ident)))
filters      <- unique(hiring_data$ident[which(hiring_data$age_finact>=50  & hiring_data$an_finact>=2000 )])
hiring_data <- hiring_data[which(is.element(hiring_data$ident,filters)),]
c(length(hiring_data$ident),length(unique(hiring_data$ident)))
hiring_data <- hiring_data[which(hiring_data$year<=hiring_data$an_finact),]
c(length(hiring_data$ident),length(unique(hiring_data$ident)))

save(hiring_data, file=paste0(path_data,"clean/hiring_data_e16_a10_v4.Rdata"))




check = hiring_data[hiring_data$ident == 1453115,]
